<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fogg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r:+&s-[dnc.-nuNe>I$}JRX:!EP:f3%H}/;!-bm}_e9g:TRaz;>+%=BQ[%RCg_Ku');
define('SECURE_AUTH_KEY',  ' 2pT6ge|W$(_I`1?KY~Z+0|~4=EYeBxoqi)AG~8-*;~;9cb5*@v/TxN$s(ukJK|M');
define('LOGGED_IN_KEY',    'Ngt?>[*EbUYJc?|tF^T&c?|Az@3g7Uw>gc`LTC#aV!539Rtsj9i7}v(rQkBnV-xJ');
define('NONCE_KEY',        'p8^T~/oW;E,1^I}HCU.oK$&=:=SjcE#U+<-5s/p,1!Id4vPA=8NZe@o/8aOy38,)');
define('AUTH_SALT',        'c2vGpO;.KkT24Kqd6<dQC6#.7T-.1k76,;PJFCHng`=j*&$T>Ws7$Or0@,z2/)ub');
define('SECURE_AUTH_SALT', 'j^nT3Q~K><NOeI_lmE7%MT4]/--eEjbi- bYP%|=x1+gFoL6XCK_OgKDJ}8`V@qJ');
define('LOGGED_IN_SALT',   'n}g46ZfoP#dTSV)*JWV/=gkF1l2Rn.}{.8MA[5,:2N)+bp1*/UpAuP_8zd5fR/a;');
define('NONCE_SALT',       'tf?6A.,=b^m2zvq^jZ8gI:(l jB=7%dEzCylaxa>%bRZ>f^4kYUr<?(>lF%{;[a[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
