<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push home">
        <!-- header -->
        <?php include("includes/header.php"); ?>

        <!-- <section id="homepage-main" style="background-image:url('/assets/images/design/homepage-background.jpg')"> -->

        <section id="homepage-main">

            <div class="container">
                <!-- <div class="homepage-main-heading small">
                    <p>Branding.</p>
                    <p>Digital.</p>
                    <p>Strategy.</p>
                    <p>Design.</p>
                    <p>Creativity.</p>
                    <p class="highlighted">Working in your world</p>
                </div> -->
            </div>
            <div class="homepage-logo-overlay">
                <!-- <img src="/assets/images/design/logo-extra-large.png" alt="fogg associates"> -->
            </div>

            <div class="scroll-down-container">
                <a href="#homepage-work" class="scroll-down-btn">Scroll <img src="/assets/images/design/icons/scroll-down-arrow-green.png" class="scroll-down-arrow" alt="scroll down"></a>
            </div>

            <video id="homepage-bg-video" width="100%" height="100%" autoplay loop mute>
              <source src="/assets/build/video/FA - Showreel 2015 SHORT WITH OVERLAY 2.mp4" type="video/mp4">
              <!-- <source src="movie.ogg" type="video/ogg"> -->
            Your browser does not support the video tag.
            </video>

        </section>


        <section id="homepage-work" class="masonry-container container">

                <div class="pod-outer single-width single-height">
                    <!-- .pod -->
                    <div class="pod message-box grey-mid-dark-bg">
                        <p>Branding.<br>
                        Digital.<br>
                        Strategy.<br>
                        Design.<br>
                        Creativity.<br>
                        </p>
                        <p class="primary-green">Working in your world.</p>
                    </div>
                    <!-- // .pod -->
                </div>

                <div class="pod-outer single-width double-height">
                    <!-- .pod -->
                    <div class="pod project-box primary-green-bg">

                        <div class="project-box-caption">
                            <div class="project-box-caption-inner">

                                <a href="#" class="project-box-close">
                                    <img src="/assets/images/design/icons/close.png" alt="close">
                                </a>

                                <div class="project-box-heading">
                                    <h3>Project name</h3>
                                </div>

                                <div class="project-box-summary">
                                    <p>From global brand strategies to start-up brand creations, international design commissions to bespoke digital solutions; regardless of the scale of project or investment, we are focused upon realising your opportunities.</p>
                                </div>

                                <a href="#" class="project-box-btn">View project</a>

                                <div class="project-box-tags">
                                    <a href="#" class="project-box-tag">Design</a>
                                    <a href="#" class="project-box-tag">Strategy</a>
                                    <a href="#" class="project-box-tag">Branding</a>
                                </div>

                            </div>
                        </div>

                        <img class="pod-background-image" src="/assets/images/content/artistic-greyscale-shot.jpg" alt="project name">
                    </div>
                    <!-- // .pod -->
                </div>

                <div class="pod-outer single-width single-height">
                    <!-- .pod -->
                    <div class="pod message-box primary-green-bg">
                        <p>Identify the opportunity.</p>
                        <p class="dark">Engineer the message.</p>
                        <p>Realise the results.</p>
                    </div>
                    <!-- // .pod -->
                </div>

                <div class="pod-outer single-width single-height">
                    <!-- .pod -->
                    <div class="pod project-box primary-yellow-bg">

                        <div class="project-box-caption">
                            <div class="project-box-caption-inner">

                                <a href="#" class="project-box-close">
                                    <img src="/assets/images/design/icons/close.png" alt="close">
                                </a>

                                <div class="project-box-heading">
                                    <h3>Project name</h3>
                                </div>

                                <div class="project-box-summary">
                                    <p>From global brand strategies to start-up brand creations, international design commissions to bespoke digital solutions; regardless of the scale of project or investment, we are focused upon realising your opportunities.</p>
                                </div>

                                <a href="#" class="project-box-btn">View project</a>

                                <div class="project-box-tags">
                                    <a href="#" class="project-box-tag">Design</a>
                                    <a href="#" class="project-box-tag">Strategy</a>
                                    <a href="#" class="project-box-tag">Branding</a>
                                </div>

                            </div>
                        </div>

                        <img class="pod-background-image" src="/assets/images/content/kelly-small-art.jpg" alt="project name">
                    </div>
                    <!-- // .pod -->
                </div>

                <div class="pod-outer single-width double-height">
                    <!-- .pod -->
                    <div class="pod project-box primary-red-bg">

                        <div class="project-box-caption">
                            <div class="project-box-caption-inner">

                                <a href="#" class="project-box-close">
                                    <img src="/assets/images/design/icons/close.png" alt="close">
                                </a>

                                <div class="project-box-heading">
                                    <h3>Project name</h3>
                                </div>

                                <div class="project-box-summary">
                                    <p>From global brand strategies to start-up brand creations, international design commissions to bespoke digital solutions; regardless of the scale of project or investment, we are focused upon realising your opportunities.</p>
                                </div>

                                <a href="#" class="project-box-btn">View project</a>

                                <div class="project-box-tags">
                                    <a href="#" class="project-box-tag">Design</a>
                                    <a href="#" class="project-box-tag">Strategy</a>
                                </div>

                            </div>
                        </div>

                        <img class="pod-background-image" src="/assets/images/content/police-image.jpg" alt="project name">
                    </div>
                    <!-- // .pod -->
                </div>
                
                <div class="pod-outer double-width single-height">
                    <!-- .pod -->
                    <div class="pod project-box primary-pink-bg">

                        <div class="project-box-caption">
                            <div class="project-box-caption-inner">

                                <a href="#" class="project-box-close">
                                    <img src="/assets/images/design/icons/close.png" alt="close">
                                </a>

                                <div class="project-box-heading">
                                    <h3>Project name</h3>
                                </div>

                                <div class="project-box-summary">
                                    <p>From global brand strategies to start-up brand creations, international design commissions to bespoke digital solutions; regardless of the scale of project or investment, we are focused upon realising your opportunities.</p>
                                </div>

                                <a href="#" class="project-box-btn">View project</a>

                                <div class="project-box-tags">
                                    <a href="#" class="project-box-tag">Design</a>
                                    <a href="#" class="project-box-tag">Strategy</a>
                                </div>

                            </div>
                        </div>

                        <img class="pod-background-image" src="/assets/images/content/kelly-landscape-art.jpg" alt="project name">
                    </div>
                    <!-- // .pod -->
                </div>

        </section>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
