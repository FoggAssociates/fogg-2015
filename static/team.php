<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push">
        <!-- header -->
        <?php include("includes/header.php"); ?>

        <section id="team" class="clear-header container standard-padding-x">

            <div id="profile-individual">
                <div class="inner-content">
                    <div class="profile-photo">
                        <img src="/assets/images/content/team.png" alt="team">
                    </div>
                    <div class="profile-details">
                        <div class="name-and-title">
                            <h3 class="name">Name</h3>
                            <p class="title">Title</p>
                            <a href="#" class="close-btn"><img src="/assets/images/design/icons/close.png" alt="close"></a>
                        </div>
                        <div class="profile-biography">
                            <p></p>
                            <!-- <p>Chris’ experience in brand development, advertising and live communications encompasses: the BBC; Toyota; Microsoft; Sony; Vodafone; Reebok; Holiday Inn; Orange; Pfizer; Doosan; KPMG; Lexus and The Shard, as well as many entrepreneurial, SMEs.</p>

                            <p>After graduating from the renowned Glasgow School of Art, Chris was invited to work at Saatchi &amp; Saatchi, London. On returning to Manchester he became Group Creative Director of an agency where he was responsible for the creative output across all disciplines. His diverse work has won multiple awards, one particular project being regarded as a ‘world first.’</p>

                            <p>In 2008 he founded Fogg Associates.</p>

                            <p>Chris uniquely marries his passion for creativity with his business and strategic acumen. He has facilitated global branding exercises and has recently consulted in developing the proposition for a global landmark development. His brand and corporate consultancy experience with business leaders enables him to truly leverage creativity as a business currency. He also frequently gives lectures and seminars to both corporate and academic audiences. Chris, amongst representatives from London’s leading design and advertising agencies, has been invited by D&amp;AD to contribute to the development of their iconic workshops.</p>

                            <p>Chris passionately believes that every brand has the opportunity to capture the essence of its market or sector, enabling it to be steadfast to its values, whilst imbuing it with an agility and continual relevance.</p> -->
                        </div>
                    </div>
                </div>
            </div>
                
            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="chris">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Chris Fogg</h3>
                        <p class="title">Founder</p>
                    </div>
                </div>
                <img src="/assets/images/content/chris.png" alt="chris">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="jim">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">James Brooke</h3>
                        <p class="title">Senior Strategist</p>
                    </div>
                </div>
                <img src="/assets/images/content/jim.png" alt="jim">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="kerry">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Kerry Howl</h3>
                        <p class="title">Client Services Director</p>
                    </div>
                </div>
                <img src="/assets/images/content/kerry.png" alt="kerry">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="steph">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Steph Meadows</h3>
                        <p class="title">Senior Designer</p>
                    </div>
                </div>
                <img src="/assets/images/content/steph.png" alt="steph">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="jack">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Jack Fairhurst</h3>
                        <p class="title">Designer</p>
                    </div>
                </div>
                <img src="/assets/images/content/jack.png" alt="jack">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="mike">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Mike Stopford</h3>
                        <p class="title">Developer</p>
                    </div>
                </div>
                <img src="/assets/images/content/mike.png" alt="mike">
            </a>
            <!-- // .team-profile-grid -->

            <!-- .team-profile-grid -->
            <a href="#" class="team-profile-grid" data-team-member="lisa">
                <div class="name-section-hover">
                    <div class="name-and-title">
                        <h3 class="name">Lisa Critchley</h3>
                        <p class="title">Administration</p>
                    </div>
                </div>
                <img src="/assets/images/content/lisa.png" alt="lisa">
            </a>
            <!-- // .team-profile-grid -->

        </section>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
