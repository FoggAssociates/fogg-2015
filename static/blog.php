<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push">
        <!-- header -->
        <?php include("includes/header.php"); ?>

        <section id="blog-latest-slider" class="clear-header">

            <div class="container">

                <!-- .individual-slide -->
                <div class="individual-slide">
                    <div class="blog-post-details">
                        <div class="blog-post-heading">
                            <h3><a href="#">From babies to brands</a></h3>
                            <p class="blog-post-author">By Steph Meadows</p>
                            <p class="blog-post-date">30.04.2015</p>
                            <a class="standard-btn green-bg" href="#">Read Post</a>
                        </div>
                    </div>

                    <div class="blog-post-image">
                        <a href="#"><img src="/assets/images/content/blog-article-latest.jpg" alt="blog article image"></a>
                    </div>
                </div>
                <!-- // .individual-slide -->

            </div>

        </section>

        <section class="double-padding-x clearfix">

            <div class="container">
                <!-- .with-sidebar -->
                <div class="col width-10 with-sidebar">
                    <!-- .blog-post -->
                    <a href="/blog-article.php" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>Branding by the Book</h3>
                                    <p class="blog-post-author">By Kerry Howl</p>
                                    <p class="blog-post-date">21.04.2015</p>
                                    <p class="blog-post-summary">Businesses come to us for many reasons: They may be a start up looking for a name as well as an identity They may be a well established organisation that is facing increasing market competition and needs...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-1.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <!-- .blog-post -->
                    <a href="#" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>The next generation</h3>
                                    <p class="blog-post-author">By Chris Fogg</p>
                                    <p class="blog-post-date">23.03.2015</p>
                                    <p class="blog-post-summary">I recently had the honour of being invited to participate in an Employability Event at Manchester Metropolitan University. The event, run by recruitment firm Creative Resource, featured more than 20...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-2.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <!-- .blog-post -->
                    <a href="#" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>Branding by the Book</h3>
                                    <p class="blog-post-author">By Kerry Howl</p>
                                    <p class="blog-post-date">21.04.2015</p>
                                    <p class="blog-post-summary">Businesses come to us for many reasons: They may be a start up looking for a name as well as an identity They may be a well established organisation that is facing increasing market competition and needs...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-3.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <!-- .blog-post -->
                    <a href="#" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>The next generation</h3>
                                    <p class="blog-post-author">By Chris Fogg</p>
                                    <p class="blog-post-date">23.03.2015</p>
                                    <p class="blog-post-summary">I recently had the honour of being invited to participate in an Employability Event at Manchester Metropolitan University. The event, run by recruitment firm Creative Resource, featured more than 20...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-4.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <!-- .blog-post -->
                    <a href="#" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>Branding by the Book</h3>
                                    <p class="blog-post-author">By Kerry Howl</p>
                                    <p class="blog-post-date">21.04.2015</p>
                                    <p class="blog-post-summary">Businesses come to us for many reasons: They may be a start up looking for a name as well as an identity They may be a well established organisation that is facing increasing market competition and needs...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-5.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <!-- .blog-post -->
                    <a href="#" class="blog-post grid-view">
                        <!-- .push-right -->
                        <div class="push-right">
                            <div class="blog-post-details blog-post-match">
                                <div class="blog-post-heading">
                                    <h3>The next generation</h3>
                                    <p class="blog-post-author">By Chris Fogg</p>
                                    <p class="blog-post-date">23.03.2015</p>
                                    <p class="blog-post-summary">I recently had the honour of being invited to participate in an Employability Event at Manchester Metropolitan University. The event, run by recruitment firm Creative Resource, featured more than 20...</p>
                                    <button class="standard-btn green-bg" href="#">Read Post</button>
                                </div>
                            </div>
                            <div class="blog-post-image blog-post-match">
                                <img src="/assets/images/content/blog-article-6.jpg" alt="blog article image">
                            </div>
                        </div>
                        <!-- // .push-right -->
                    </a>
                    <!-- .blog-post -->

                    <a href="#" class="load-more-posts">Load more</a>
                </div>
                <!-- // .with-sidebar -->

                <!-- .sidebar -->
                <div class="col width-2 sidebar">
                    <div class="sidebar-area">
                        <h3>Search Blog</h3>
                        <form id="blog-search">
                            <input type="text" id="blog-search-input" placeholder="Search blog...">
                            <input type="submit" value="SEARCH" class="standard-btn green-bg" id="blog-search-submit">
                        </form>
                    </div>
                    <div class="sidebar-area">
                        <h3>Latest posts</h3>
                        <ul>
                            <li><a href="#">From babies to brands</a></li>
                            <li><a href="#">Branding by the Book</a></li>
                            <li><a href="#">The next generation</a></li>
                            <li><a href="#">We’re in the top 30!</a></li>
                        </ul>
                    </div>
                    <div class="sidebar-area">
                        <h3>Archive</h3>
                        <ul>
                            <li><a href="#">2014</a></li>
                            <li><a href="#">2013</a></li>
                            <li><a href="#">2012</a></li>
                            <li><a href="#">2011</a></li>
                            <li><a href="#">2010</a></li>
                            <li><a href="#">2009</a></li>
                            <li><a href="#">2008</a></li>
                        </ul>
                    </div>
                </div>
                <!-- // .sidebar -->
            </div>

        </section>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
