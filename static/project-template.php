<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push project-page">
        <!-- header -->
        <?php include("includes/header.php"); ?>

        <section class="clear-header" id="project-masthead">

            <img src="/assets/images/content/shard-masthead.jpg" alt="the shard">

        </section>

        <section class="double-padding-x clearfix">

            <div class="container">
               
               <div class="project-title">
                    <h2 class="project-name">The Shard</h2>
                    <p class="project-tagline">Brand innovation for a global landmark</p>
                    <div class="project-tags">
                    	<a href="#">Branding</a>
                    	<a href="#">Brochure Design</a>
                    </div>
               </div>

               <div class="project-story">
					
					<!-- .row -->
	                <div class="row">
						<div class="col width-6">
							<p>Two years before the completion of this 12-year mega-development, the management team of London Bridge Quarter briefed Fogg to develop a bespoke Tenant Handbook for The Shard.</p>
						</div>

						<div class="col width-6">
							<p>We approached the design articulation exploration from an architectural standpoint before we deployed our graphic design sensibilities.</p>
						</div>
					</div>
					<!-- // .row -->
	
					<!-- .row -->
					<div class="row">
						<div class="col width-12">
							<img class="full-width" src="/assets/images/content/the-shard-3-shots.jpg">
						</div>
					</div>
					<!-- // .row -->

					<!-- .row -->
					<div class="row">
						<div class="col width-6">
							<img class="full-width" src="/assets/images/content/the-shard-half-1.jpg">
						</div>
						<div class="col width-6">
							<img class="full-width" src="/assets/images/content/the-shard-half-2.jpg">
						</div>
					</div>
					<!-- // .row -->

					<!-- .row -->
					<div class="row">
						<div class="col width-12 quote-block" style="background-image: url('/assets/images/content/shard-sky-image.jpg')">
							<p>When building a service proposition, most people build it from the ground up. We started with the highest level of service, them build upon them. We refer to this approach as... <span class="bold">from the sky up.</span></p>
						</div>
					</div>
					<!-- // .row -->

					<!-- .row -->
					<div class="row">
						<div class="col width-12 text-info-area text-left">
							<div class="text">
								<h3>The process</h3>
								<p>Observing the distinctive angles and layered facets from the drawings by architect Renzo Piano, we began to explore how these elements could form graphic design motifs.</p>
								<p>We also explored a minimal tonal and material palette to evoke the lightness of the architecture.</p>
								<p>Inspired by our visits to the building during its construction phase, observing how the building’s glass skin seemed to float invisibly against the building’s super-structure behind, we designed innovative binding mechanics where the outer cover opened asymmetrically against the inner binding.</p>
							</div>
							<div class="image">
								<img src="/assets/images/content/shard-drawing.gif">
							</div>
						</div>
					</div>
					<!-- // .row -->

					<!-- .row -->
					<div class="row">
						<div class="col width-12">
							<img src="/assets/images/content/shard-screens.png">
						</div>
					</div>
					<!-- // .row -->

					<!-- .row -->
					<div class="row">
						<div class="col width-6">
							<img class="full-width" src="/assets/images/content/shard-web-1.jpg">
						</div>
						<div class="col width-6">
							<img class="full-width" src="/assets/images/content/shard-web-2.jpg">
						</div>
					</div>
					<!-- // .row -->

               </div>
                    
            </div>

        </section>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
