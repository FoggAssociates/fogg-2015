<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push">
        <!-- header -->
        <?php include("includes/header.php"); ?>


        <section class="clearfix contact-us clear-header">

            <div id="contact-masthead">
                <img class="full-width" src="/assets/images/design/contact-masthead.png" alt="fogg associates">
            </div>

            <div class="container double-padding-x">

                <!-- .col -->
                <div class="col width-3">

                    <!-- .contact-area -->
                    <div class="contact-area">
                        <h4>Contacting Us</h4>
                        <p>Call us at the studio on:</p>
                        <p>+44 (0)1925­ 22 61 39</p>
                        <p>selecting #1 for the studio</p>
                        <p>selecting #2 for financial &amp; admin</p>

                        <p>or drop us an email at:</p>
                        <a href="mailto:info@foggassociates.com">info@foggassociates.com</a>

                        <p>for administration email us at:</p>
                        <a href="mailto:admin@foggassociates.com">admin@foggassociates.com</a>
                    </div>
                    <!-- // .contact-area -->

                    <!-- .contact-area -->
                    <div class="contact-area">
                        <h4>Recruitment</h4>
                        <p>We welcome pdf portfolios and CVs via email on the first Thursday of every month. Send to:</p>
                        <a href="mailto:info@foggassociates.com">info@foggassociates.com</a>
                    </div>
                    <!-- //.contact-area -->

                </div>
                <!-- // .col -->

                <!-- .col -->
                <div class="col width-3">
                
                    <!-- .contact-area -->
                    <div class="contact-area">
                        <h4>Finding Us</h4>
                        <p>If you would like to visit, sat nav:</p>
                        <p>WA12 9SG</p>

                        <p>Please note our mews is just off the High Street. We have on-site parking.</p>

                        <p>Fogg Associates<br>
                        152a The Elms<br>
                        High Street<br>
                        Newton le Willows<br>
                        WA12 9SG</p>
                    </div>
                    <!-- // .contact-area -->

                </div>
                <!-- // .col -->

                <!-- .col -->
                <div class="col width-3">
                
                    <!-- .contact-area -->
                    <div class="contact-area">
                        <h4>UK Travel</h4>
                        <p>Trains:</p>
                        <p>We are a 10 min taxi journey from Warrington Bank Quay station, therefore only 1:45 hours from London Euston.</p>

                        <p>Or our nearest station, Newton-Le-Willows is only a few minutes walk away.</p>

                        <a href="#">Virgin Trains</a>
                        <a href="#">Trainline</a>

                        <p>Motorways:</p>
                        <p>The village where we are based is conveniently located within a mile of the juncture of the M6, M62 and M56.</p>

                        <p>From the North and South, J23 M6. From East and West J9 M62.</p>

                        <a href="#">Plan your route</a>
                    </div>
                    <!-- // .contact-area -->

                </div>
                <!-- // .col -->

                <!-- .col -->
                <div class="col width-3">
                
                    <!-- .contact-area -->
                    <div class="contact-area">
                        <h4>International Travel</h4>
                        <p>We are based equidistant from Manchester and Liverpool – 20 mins from either and their respective international airports.</p>

                        <p>Airports:</p>
                        <a href="#">Manchester International</a>
                        <a href="#">Liverpool John Lennon</a>

                        <p>Overnight accommodation:</p>
                        <p>There are a number of hotels within 2 miles of our office if you are planning an overnight stay.</p>

                        <a href="#">Holiday Inn</a>
                        <a href="#">Thistle</a>
                        <a href="#">Premier Inn</a>
                        <a href="#">Ramada Encore</a>
                        <a href="#">De Vere Village</a>
                        <a href="#">QHotels</a>
                    </div>
                    <!-- // .contact-area -->

                </div>
                <!-- // .col -->

            </div>

            <div class="scroll-down-container">
                <a href="#map-canvas" class="scroll-down-btn">Map <img src="/assets/images/design/icons/scroll-down-arrow-green.png" class="scroll-down-arrow" alt="scroll down"></a>
            </div>

        </section>

        <div id="map-canvas">
            <div id="map-loader">
                <!-- google maps javascript api loader -->
            </div>
            <div id="gradient"></div>
        </div>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
