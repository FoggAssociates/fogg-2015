<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <title>Fogg Associates</title>

        <!-- meta data and stylesheets -->
        <?php include("includes/meta.php"); ?>
    </head>
    <body class="cbp-spmenu-push blog-article">
        <!-- header -->
        <?php include("includes/header.php"); ?>

        <section class="standard-padding-x clearfix clear-header">

            <div class="container">
                <!-- .with-sidebar -->
                <div class="col width-10 with-sidebar">

                    <div class="blog-post-image">
                        <img src="/assets/images/content/blog-article-1-main.jpg" alt="blog article image">
                    </div>

                    <div class="blog-post-details">
                        <div class="blog-post-heading">
                            <h3>Branding by the Book</h3>
                            <p class="blog-post-author">By Kerry Howl</p>
                            <p class="blog-post-date">21.04.2015</p>
                        </div>
                        <div class="blog-post-content single-col">
                            <p>Businesses come to us for many reasons: They may be a start up looBusinesses come to us for many reasons:</p>

                            <p>They may be a start up looking for a name as well as an identity</p>

                            <p>They may be a well established organisation that is facing increasing market competition and needs to create stand</p>

                            <p>They may be an organisation that is increasing it’s service and products and therefore needs to expand it’s brand portfolio</p>

                            <p>Or it may be a business that simply needs to refresh it’s identity and improve it’s awareness</p>

                            <p>These are just a few examples of the challenges our clients face. Over our 8 year existence we have developed a proven and successful way to help create or reposition a brand. But we always have to consider that when we create something, we have to protect it as well.</p>

                            <p>So on the 15th April 2015 we teamed up with Marks &amp; Clerk, A group of patent and trade mark attorney firms with offices in Europe, Canada and Asia. Together we delivered a seminar to talk people through the process of creating and protecting a brand.</p>

                            <p>The event was held at Daresbury Sci Tech home to over 100 high-tech companies employing nearly 500 people in areas such as advanced engineering, digital/ICT, biomedical and energy and environmental technologies.</p>

                            <p>But the event attracted many businesses from across the North West representing a variety industries.</p>

                            <p>The presentation was delivered in two sections. I delivered the Creation stage, which was then followed by Mike Shaw from Marks and Clerk who took us through how to protect your brand.</p>

                            <p>The format of the presentation was based on a step by step guide to really simplify the branding process and encourage businesses to dig deep to explore their unique market positioning. It highlighted how a brand needs to be built on a clearly defined message, to support the vision of a business and drive it’s culture.  Some examples were given of brands that successfully ‘owned’ a clear message and applied it to every aspect of their brand communication, both internally and externally. I used Volvo to demonstrate how they don’t just apply ‘Safety’ to their cars but use it to drive all their marketing. They ‘Own’ Safety!</p>

                            <p>Mike then presented a step by step guide on how a brand should be protected, using some real life case studies to demonstrate all aspects of IP and Trademarking. I personally found it incredibly interesting the number of things you can actually protect. Did you know that a company that makes Tennis Ball has protected the smell of ‘cut grass’ in it’s development.</p>

                            <p>And once we were done with the formalities there was an open Q&amp;A session where Mike and I were ‘grilled’ on our respective elements of the presentation. These questions really highlighted some of the brand creation and protection issues businesses of all sizes face. So we were really happy to help!</p>

                            <p>If you would like to find out more about the seminar then please drop me a line. We’d be happy to give you a private run through of the presentation and discuss your own individual brand challenges. Businesses come to us for many reasons:</p>
                        </div>
						<div class="blog-post-content single-col">
                            <p>Businesses come to us for many reasons: They may be a start up looBusinesses come to us for many reasons:</p>

                            <p>They may be a start up looking for a name as well as an identity</p>

                            <p>They may be a well established organisation that is facing increasing market competition and needs to create stand</p>

                            <p>They may be an organisation that is increasing it’s service and products and therefore needs to expand it’s brand portfolio</p>

                            <p>Or it may be a business that simply needs to refresh it’s identity and improve it’s awareness</p>

                            <p>These are just a few examples of the challenges our clients face. Over our 8 year existence we have developed a proven and successful way to help create or reposition a brand. But we always have to consider that when we create something, we have to protect it as well.</p>

                            <p>So on the 15th April 2015 we teamed up with Marks &amp; Clerk, A group of patent and trade mark attorney firms with offices in Europe, Canada and Asia. Together we delivered a seminar to talk people through the process of creating and protecting a brand.</p>

                            <p>The event was held at Daresbury Sci Tech home to over 100 high-tech companies employing nearly 500 people in areas such as advanced engineering, digital/ICT, biomedical and energy and environmental technologies.</p>

                            <p>But the event attracted many businesses from across the North West representing a variety industries.</p>

                            <p>The presentation was delivered in two sections. I delivered the Creation stage, which was then followed by Mike Shaw from Marks and Clerk who took us through how to protect your brand.</p>

                            <p>The format of the presentation was based on a step by step guide to really simplify the branding process and encourage businesses to dig deep to explore their unique market positioning. It highlighted how a brand needs to be built on a clearly defined message, to support the vision of a business and drive it’s culture.  Some examples were given of brands that successfully ‘owned’ a clear message and applied it to every aspect of their brand communication, both internally and externally. I used Volvo to demonstrate how they don’t just apply ‘Safety’ to their cars but use it to drive all their marketing. They ‘Own’ Safety!</p>

                            <p>Mike then presented a step by step guide on how a brand should be protected, using some real life case studies to demonstrate all aspects of IP and Trademarking. I personally found it incredibly interesting the number of things you can actually protect. Did you know that a company that makes Tennis Ball has protected the smell of ‘cut grass’ in it’s development.</p>

                            <p>And once we were done with the formalities there was an open Q&amp;A session where Mike and I were ‘grilled’ on our respective elements of the presentation. These questions really highlighted some of the brand creation and protection issues businesses of all sizes face. So we were really happy to help!</p>

                            <p>If you would like to find out more about the seminar then please drop me a line. We’d be happy to give you a private run through of the presentation and discuss your own individual brand challenges. Businesses come to us for many reasons:</p>
                        </div>
                    </div>

                </div>
                <!-- // .with-sidebar -->

                <!-- .sidebar -->
                <div class="col width-2 sidebar">
                    <div class="sidebar-area">
                        <h3>Search Blog</h3>
                        <form id="blog-search">
                            <input type="text" id="blog-search-input" placeholder="Search blog...">
                            <input type="submit" value="SEARCH" class="standard-btn green-bg" id="blog-search-submit">
                        </form>
                    </div>
                    <div class="sidebar-area">
                        <h3>Latest posts</h3>
                        <ul>
                            <li><a href="#">From babies to brands</a></li>
                            <li><a href="#">Branding by the Book</a></li>
                            <li><a href="#">The next generation</a></li>
                            <li><a href="#">We’re in the top 30!</a></li>
                        </ul>
                    </div>
                    <div class="sidebar-area">
                        <h3>Archive</h3>
                        <ul>
                            <li><a href="#">2014</a></li>
                            <li><a href="#">2013</a></li>
                            <li><a href="#">2012</a></li>
                            <li><a href="#">2011</a></li>
                            <li><a href="#">2010</a></li>
                            <li><a href="#">2009</a></li>
                            <li><a href="#">2008</a></li>
                        </ul>
                    </div>
                </div>
                <!-- // .sidebar -->
            </div>

        </section>

        <!-- footer -->
        <?php include("includes/footer.php"); ?>

        <!-- scripts -->
        <?php include("includes/scripts.php"); ?>
    </body>
</html>
