$(document).ready(function(){

  $('.col').matchHeight();
  $('.blog-post-match').matchHeight();

  headerSearchFocus();
  homepageMainSection();
  smoothScroll();
  projectBoxCaption();
  mobileMenu();
  teamMemberProfiles();

  var $container = $('.masonry-container');
  // initialize
  $container.masonry({
    itemSelector: '.pod-outer'
  });

  var homepageVideo = $('#homepage-bg-video');

});

$(window).load(function(){

  var scroll = $(window).scrollTop();
	var windowWidth = $(window).width();

   //>=, not <=
  if (scroll >= 20 && windowWidth > 600) {
    $('header').addClass('scrolled');
  }

});

$(window).resize(function(){

  homepageMainSection();

});

$(window).scroll(function() {
	var scroll = $(window).scrollTop();
  var windowWidth = $(window).width();

   // >=, not <=
  if (scroll >= 20 && windowWidth > 600) {
  	$('header').addClass('scrolled');
  } else {
  	$('header').removeClass('scrolled');
  }

}); // END window.scroll();


/* --------------------------------------------------------------
functions
-------------------------------------------------------------- */



/* check for valid email address */
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}

function headerSearchFocus() {

  var searchBar = $('#header-search');
  var searchInput = searchBar.find('input[type=text]');

  searchInput.on('focus', function() {
    searchBar.addClass('focus');
  });

  searchInput.on('focusout', function() {
    if ( searchInput.val() == '') {
        searchBar.removeClass('focus');
    }
  });

}

function homepageMainSection() {

  var windowHeight = $(window).height();
  var windowWidth = $(window).width();
  var mainSection = $('#homepage-main');

  if (windowWidth >= 600) {
    mainSection.css({
      'height' : windowHeight
    });
  }

}

function smoothScroll() {

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

}

function projectBoxCaption() {

  var caption = $('.project-box-caption');
  var project = $('.project-box');
  var closeBtn = $('.project-box-close');

  project.on('click', function() {
    // project.find(caption).fadeOut();
    $(this).find(caption).fadeIn();
  });

  closeBtn.on('click',function(e) {
    e.stopPropagation();
    e.preventDefault();
    // $(this).closest(caption).fadeOut();
    // 
    $(this).closest(caption).animate({'top':'100%'}, 300, 'easeInBack').fadeOut().animate({'top':'0'});

  });

  closeBtn.on('mousedown', function() {
    $(this).find('img').animate({width:"20px", height:"20px"},100);
  });
  closeBtn.on('mouseup', function() {
    $(this).find('img').animate({width:"30px", height:"30px"},100);
  });

}

function mobileMenu() {
  var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
    showRightPush = document.getElementById( 'showRightPush' ),
    body = document.body;

  showRightPush.onclick = function() {
    classie.toggle( this, 'active' );
    classie.toggle( body, 'cbp-spmenu-push-toleft' );
    classie.toggle( menuRight, 'cbp-spmenu-open' );

  };

}

function initializeMap() {

  var locations = {

    fogg: {
      lat: 53.458521,
      lang: -2.622164
    }

  }

  var markerImage = '/assets/images/design/icons/fogg-marker-2.png';

  var locationSelected = $(this).data('location');

  var mapCanvas = document.getElementById('map-loader');

  var mapOptions = {
    center: new google.maps.LatLng(locations.fogg.lat, locations.fogg.lang),
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    scaleControl: false,
    panControl: false,
    streetViewControl: false
  }

  var map = new google.maps.Map(mapCanvas, mapOptions);

  var marker = new google.maps.Marker({
    position: mapOptions.center,
    map: map,
    title: 'Fogg Associates',
    animation: google.maps.Animation.DROP,
    icon: markerImage
  });

  map.set('styles', [
    {
      featureType:'road',
      elementType: 'geometry',
      stylers: [
        { color: '#f8f6f6' },
        { weight: 0.8 }
      ]
    },
    {
      featureType:'road',
      elementType: 'labels',
      stylers: [
        { saturation: 0 },
        { color: '#8ec44a' },
        { weight: 0.2 }
      ]
    },
    {
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [
          { color: '#ffffff' },
        ]
    },
    {
        featureType: 'poi.business',
        elementType: 'labels',
        stylers: [
          { color: '#292961' },
          { weight: 0.4 },
          { visibility: 'off' }
        ]
    }
  ]); 

};

google.maps.event.addDomListener(window, 'load', initializeMap); 


function teamMemberProfiles() {

  var bios = {
    chris: '<p>Chris’ experience in brand development, advertising and live communications encompasses: the BBC; Toyota; Microsoft; Sony; Vodafone; Reebok; Holiday Inn; Orange; Pfizer; Doosan; KPMG; Lexus and The Shard, as well as many entrepreneurial, SMEs.</p>

      <p>After graduating from the renowned Glasgow School of Art, Chris was invited to work at Saatchi &amp; Saatchi, London. On returning to Manchester he became Group Creative Director of an agency where he was responsible for the creative output across all disciplines. His diverse work has won multiple awards, one particular project being regarded as a ‘world first.’</p>

      <p>In 2008 he founded Fogg Associates.</p>

      <p>Chris uniquely marries his passion for creativity with his business and strategic acumen. He has facilitated global branding exercises and has recently consulted in developing the proposition for a global landmark development. His brand and corporate consultancy experience with business leaders enables him to truly leverage creativity as a business currency. He also frequently gives lectures and seminars to both corporate and academic audiences. Chris, amongst representatives from London’s leading design and advertising agencies, has been invited by D&amp;AD to contribute to the development of their iconic workshops.</p>

      <p>Chris passionately believes that every brand has the opportunity to capture the essence of its market or sector, enabling it to be steadfast to its values, whilst imbuing it with an agility and continual relevance.</p>',
      jim: '<p>jim bio</p>',
      kerry: '<p>kerry bio</p>',
      steph: '<p>steph bio</p>',
      jack: '<p>jack bio</p>',
      mike: '<p>mike bio</p>',
      lisa: '<p>lisa bio</p>'
  }

  var profilePhoto = $('.team-profile-grid');
  var profileCanvas = $('#profile-individual');
  var profileCanvasInner = $('#profile-individual .inner-content');
  var profileBio = $('.profile-biography');

  var closeBtn = $('.close-btn');

  var profileSelected;

  profilePhoto.on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();
    profileCanvas.fadeIn().addClass('open');

    var memberName = $(this).find('.name').text();
    var memberTitle = $(this).find('.title').text();

    var member = $(this).data('team-member');
    var memberPhotoSrc = $('.profile-photo > img').attr('src');
    var memberPhotoAlt = $('.profile-photo > img').attr('alt');
    var memberImage = $('.profile-photo > img');

    var individualMemberName = profileCanvas.find('.name');
    var individualMemberTitle = profileCanvas.find('.title');

    individualMemberName.text(memberName);
    individualMemberTitle.text(memberTitle);

    if ( $(this).data('team-member') === 'chris' ) {

      profileBio.html(bios.chris);

    } else if ( $(this).data('team-member') === 'jim' ) {

      profileBio.html(bios.jim);

    } else if ( $(this).data('team-member') === 'kerry' ) {

      profileBio.html(bios.kerry);

    } else if ( $(this).data('team-member') === 'steph' ) {

      profileBio.html(bios.steph);

    } else if ( $(this).data('team-member') === 'jack' ) {

      profileBio.html(bios.jack);

    } else if ( $(this).data('team-member') === 'mike' ) {

      profileBio.html(bios.mike);

    } else if ( $(this).data('team-member') === 'lisa' ) {

      profileBio.html(bios.lisa);

    }

    memberImage.attr('alt', member);
    memberImage.attr('src', '/assets/images/content/' + member + '.png');

  });

  $('body').on('click', function() {
    profileCanvas.fadeOut().removeClass('open');
  });

  profileCanvasInner.on('click', function(e) {
    e.stopPropagation();
  });

  closeBtn.on('click', function(e) {
    e.preventDefault();
    profileCanvas.fadeOut().removeClass('open');
  });

}