<!-- footer -->
<footer>
	<div class="container">
		<div class="col width-3">
			<p class="heading">Fogg Associates</p>
			<p>UK based international brand development firm. Est. 2008</p>
		</div>

		<div class="col width-3">
			
			<div class="col width-6">
				<ul class="footer-links">
					<li><a href="/">Home</a></li>
					<li><a href="/about.php">About</a></li>
					<li><a href="/work.php">Work</a></li>
				</ul>
			</div>

			<div class="col width-6">
				<ul class="footer-links">
					<li><a href="/team.php">Team</a></li>
					<li><a href="/blog.php">Blog</a></li>
					<li><a href="/contact.php">Contact</a></li>
				</ul>
			</div>

		</div>

		<div class="col width-3">
			<p class="heading">The Elms, 152a High Street, WA12 9SG</p>
			<p>T. +44 (0) 1925 226 139</p>
			<p>E. info@foggassociates.com</p>
		</div>

		<div class="col width-3">
			<p class="heading">SHARE, FOLLOW &amp; CONNECT</p>
			<a href="#" class="footer-social"><span class="icon-twitter"></span></a>
			<a href="#" class="footer-social"><span class="icon-feed2"></span></a>
			<a href="#" class="footer-social"><span class="icon-linkedin2"></span></a>
			<a href="#" class="footer-social"><span class="icon-youtube4"></span></a>
		</div>
	</div>
</footer>
<!-- END footer -->