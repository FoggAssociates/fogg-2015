<div class="float-right" id="showRightPush">
	<div class="horizontal-bar"></div>
	<div class="horizontal-bar"></div>
	<div class="horizontal-bar"></div>
</div>
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
	<h3>Menu</h3>
	<li class="active"><a href="/">Home</a></li>
	<li><a href="/work.php">Work</a></li>
	<li><a href="/team.php">Team</a></li>
	<li><a href="/blog.php">Blog</a></li>
	<li><a href="/contact.php">Contact</a></li>
</nav>
<!-- header -->
<header>
	<div class="container">

		<!-- .float-left -->
		<div class="float-left">
			<a href="/" class="logo"><img src="/assets/images/design/logo.png" alt="fogg associates"></a>
		</div>

		<!-- .float-right -->
		<div class="float-right">
			<nav id="header-navigation">
				<ul>
					<li class="active"><a href="/"><span class="icon-home3"></span></a></li>
					<li><a href="/work.php">Work</a></li>
					<li><a href="/team.php">Team</a></li>
					<li><a href="/blog.php">Blog</a></li>
					<li><a href="/contact.php">Contact</a></li>
				</ul>
			</nav>
			<form id="header-search">
				<input type="text" placeholder="Search..." name="search" id="header-search-bar">
				<input type="submit" value="" id="header-search-btn">
			</form>
		</div>

	</div>
</header>
<!-- END header -->