<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/src/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwnnw3E0Zl12BAB4_TW7l_9vuWyNUNEd8"></script>
<script src="assets/src/js/vendor/jquery.matchHeight.min.js"></script>
<script src="assets/build/js/min/masonry.pkgd.min.js"></script>
<script src="assets/build/js/min/jquery-ui.min.js"></script>
<script src="assets/build/js/min/classie.js"></script>
<script src="assets/build/js/min/main.min.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script>