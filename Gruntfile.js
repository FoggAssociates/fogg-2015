module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

   /**
   * Set project object
   */
    project: {
      app: '/Users/mikestopford/server/sites/Fogg-2015/wp-content/themes/fogg',
      assets: '<%= project.app %>/assets',
      src: '<%= project.assets %>/src',
      css: [
        '<%= project.src %>/scss/main.scss'
      ],
      js: [
        '<%= project.src %>/js/*.js'
      ]
    },

    /**
     * Project banner
     */
    tag: {
      banner: '/*!\n' +
              ' * <%= pkg.name %>\n' +
              ' * <%= pkg.title %>\n' +
              ' * <%= pkg.url %>\n' +
              ' * @author <%= pkg.author %>\n' +
              ' * @version <%= pkg.version %>\n' +
              ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
              ' */\n'
    },

    /**
     * Sass
     */
    sass: {
      dev: {
        options: {
          style: 'expanded',
          banner: '<%= tag.banner %>',
          compass: true
        },
        files: {
          '<%= project.assets %>/build/css/main.css': '<%= project.css %>'
        }
      },
      dist: {
        options: {
          style: 'compressed',
          compass: true
        },
        files: {
          '<%= project.assets %>/build/css/main.css': '<%= project.css %>'
        }
      }
    },

    jshint: {
         files: ['Gruntfile.js', '<%= project.assets %>src/js/*.js', '<%= project.assets %>src/js/modules/*.js'],
         options: {
             globals: {
                  jQuery: true,
                  console: true,
                  module: true
             }
         }
    },

    /**
     * Uglify JS
     */
      uglify: {
        my_target: {
          files: [{
              expand: true,
              flatten:true,
              cwd: '<%= project.assets %>/src/js',
              src: '*.js',
              dest: '<%= project.assets %>/build/js/min',
              ext: '.min.js'
          }]
        }
      },

    /**
     * Watch
     */
    watch: {
      sass: {
        files: '<%= project.src %>/scss/{,*/}*.{scss,sass}',
        tasks: ['sass:dev'],
        options: {
          livereload: true
        }
      },
      js: { 
        files: '<%= project.assets %>/src/js/*.js', 
        tasks: [ 'uglify' ],
        options: {
          livereload: true
        }
      }
    }
  });

  /**
   * Load Grunt plugins
   */
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  /**
   * Default task
   * Run `grunt` on the command line
   */
  grunt.registerTask('default', [
    'sass:dev',
    'watch',
    'uglify'
  ]);

};