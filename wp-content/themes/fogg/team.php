<?php 
    get_template_part('/shared/header');
    /*
        Template name: Team
     */
?>

<section id="team" class="clear-header container standard-padding-x">
    <?php

        $args = array(
            'post_type' => 'fogg_team'
        );

        $loop = new WP_Query($args);

        if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();

        $name = get_the_title();
        $nameLower = strtolower($name);
        $displayName = str_replace(' ', '-', $nameLower);

    ?> 
        <div class="profile-individual" data-team-member-individual="<?php echo $displayName; ?>">
            <div class="inner-content">
                <div class="container">
                    <div class="profile-photo">
                        <img src="<?php the_field('team_member_photo') ?>" alt="<?php the_title(); ?>">
                    </div>
                    <div class="profile-details">
                        <div class="name-and-title">
                            <h3 class="name"><?php the_title(); ?></h3>
                            <p class="title"><?php the_field('team_member_title'); ?></p>
                            <a href="#" class="close-btn"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/design/icons/close.png" alt="close"></a>
                        </div>
                        <div class="profile-biography">
                            <?php the_field('team_member_bio'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile;
            endif;
        wp_reset_query();
    ?>

    <?php

        $args = array(
            'post_type' => 'fogg_team'
        );

        $loop = new WP_Query($args);

        if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();

        $name = get_the_title();
        $nameLower = strtolower($name);
        $displayName = str_replace(' ', '-', $nameLower);

    ?> 
        
        <!-- .team-profile-grid -->
        <a href="#" class="team-profile-grid" data-team-member="<?php echo $displayName; ?>">
            <div class="name-section-hover">
                <div class="name-and-title">
                    <h3 class="name"><?php the_title(); ?></h3>
                    <p class="title"><?php the_field('team_member_title'); ?></p>
                </div>
            </div>
            <img src="<?php the_field('team_member_photo'); ?>" alt="<?php the_title(); ?>">
        </a>
        <!-- // .team-profile-grid -->

    <?php endwhile;
            endif;
        wp_reset_query();
    ?>

</section>

<?php get_template_part('/shared/footer'); ?>