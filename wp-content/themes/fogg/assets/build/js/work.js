jQuery(document).ready(function($) {
    $('.work-filters :checkbox').change(function() {

        $("#loading-image").addClass('visible');

        var selectedTax = checkCheckboxes();
        data = {
            action: 'workfilter',
            work_nonce: work_vars.work_nonce, // wp_nonce
            taxonomy:  selectedTax
        };
        
        $.post(
            work_vars.work_ajax_url,
            data,
            function(response){
                $("#loading-image").removeClass('visible');
                $('.work-holder').html(response);
                //console.log( 'responsed: ' + response );
            }
        );

    });
});

function checkCheckboxes(){
    var selectedTax = [];
    $('.work-filters :checkbox').each(function() {
        if (this.checked) {
            selectedTax.push($(this).attr('id'));
        }
    });

    selectedTax = JSON.stringify(selectedTax);
    return selectedTax;
}