(function($){

	jQuery(function($){

		previewVideoInAdmin();
		projectAdminColours();
		
	});

	function previewVideoInAdmin() {
		
		var promoVideoContainer = $('#acf-special_promo_video');
		var projectVideoContainer = $('#acf-project_video_embed');
		var promoVideoInput = promoVideoContainer.find('input');
		var projectVideoInput = projectVideoContainer.find('input');
		var promoVideoInputVal = promoVideoInput.val();
		var projectVideoInputVal = projectVideoInput.val();
		promoVideoContainer.append('<div class="video-preview"></div>');
		projectVideoContainer.append('<div class="video-preview-2"></div>');

		var videoPreview = $('.video-preview');
		var videoPreviewTwo = $('.video-preview-2');

		if (promoVideoContainer.length > 0) {

			if (promoVideoInputVal != '' && promoVideoInputVal.indexOf('http') == 0) {
				videoPreview.append('<iframe class="preview-video" width="100%" height="300" src="' + promoVideoInputVal + '?rel=0" frameborder="0" allowfullscreen></iframe>');
			}
		}

		if (projectVideoContainer.length > 0) {

			if (projectVideoInputVal != '' && projectVideoInputVal.indexOf('http') == 0) {
				videoPreviewTwo.append('<iframe class="preview-video" width="100%" height="300" src="' + projectVideoInputVal + '?rel=0" frameborder="0" allowfullscreen></iframe>');
			}
		}

		if (promoVideoContainer.length > 0) {
		
			promoVideoInput.on('keyup', function() {

				var promoVideoInputVal = promoVideoInput.val();

				videoPreview.empty();
				if (promoVideoInputVal != '' && promoVideoInputVal.indexOf('http') == 0) {
					videoPreview.append('<iframe class="preview-video" width="100%" height="300" src="' + promoVideoInputVal + '?rel=0" frameborder="0" allowfullscreen></iframe>');
				}
			});
		}

		if (projectVideoContainer.length > 0) {

			projectVideoInput.on('keyup', function() {

				var projectVideoInputVal = projectVideoInput.val();

				videoPreviewTwo.empty();
				if (projectVideoInputVal != '' && projectVideoInputVal.indexOf('http') == 0) {
					videoPreviewTwo.append('<iframe class="preview-video" width="100%" height="300" src="' + projectVideoInputVal + '?rel=0" frameborder="0" allowfullscreen></iframe>');
				}

			});
		}

	}

	function projectAdminColours() {
		var colorContainer = $('#acf-project_colour');
		var colours = colorContainer.find('input');
		var selectedColour = colorContainer.find('input:checked');
		var inputFields = $('.postbox .field');

		var primarygreen = '#8ec44a';
		var greymiddark = '#858687';
		var primarypink = '#e8337d';
		var primaryblue = '#292961';
		var primaryred = '#b80d1f';
		var primaryyellow = '#f6c900';
		var primaryorange = '#ff6b00';
		var primarylightblue = '#33bcff';
		var primarypurple = '#9622b2';
		var primarymint = '52db9c';
		var primarylightorange = '#4bb03b';
		var primaryturquoise = '#00a99d';
		var primarylightpink = '#ff7bac';
		var primarybeige = '#c69c6d';
		var primaryeverywalk = '#bf0552';

		
		if (selectedColour.val() == 'primary-pink') {

			inputFields.css({

				borderColor : primarypink

			});
		} else if (selectedColour.val() == 'grey-dark') {

			inputFields.css({

				borderColor : greymiddark

			});
		} else if (selectedColour.val() == 'primary-blue') {

			inputFields.css({

				borderColor : primaryblue

			});
		} else if (selectedColour.val() == 'primary-green') {

			inputFields.css({

				borderColor : primarygreen

			});
		} else if (selectedColour.val() == 'primary-red') {

			inputFields.css({

				borderColor : primaryred

			});
		} else if (selectedColour.val() == 'primary-yellow') {

			inputFields.css({

				borderColor : primaryyellow

			});
		} else if (selectedColour.val() == 'primary-orange') {

			inputFields.css({

				borderColor : primaryorange

			});
		} else if (selectedColour.val() == 'primary-lightblue') {

			inputFields.css({

				borderColor : primarylightblue

			});
		} else if (selectedColour.val() == 'primary-purple') {

			inputFields.css({

				borderColor : primarypurple

			});
		} else if (selectedColour.val() == 'primary-mint') {

			inputFields.css({

				borderColor : primarymint

			});
		} else if (selectedColour.val() == 'primary-lightorange') {

			inputFields.css({

				borderColor : primarylightorange

			});
		} else if (selectedColour.val() == 'primary-turquoise') {

			inputFields.css({

				borderColor : primaryturquoise

			});
		} else if (selectedColour.val() == 'primary-lightpink') {

			inputFields.css({

				borderColor : primarylightpink

			});
		} else if (selectedColour.val() == 'primary-beige') {

			inputFields.css({

				borderColor : primarybeige

			});
		} else if (selectedColour.val() == 'primary-everywalk') {

			inputFields.css({

				borderColor : primaryeverywalk

			});
		}

		colours.on('click', function() {

			var colorAttr = $(this).attr('value');
			var colorAttrStripped = colorAttr.replace('-', '');

			console.log(colorAttrStripped);

			if (colorAttr == 'primary-pink') {

				inputFields.css({

					borderColor : primarypink

				});
			} else if (colorAttr == 'grey-dark') {

				inputFields.css({

					borderColor : greymiddark

				});
			} else if (colorAttr == 'primary-blue') {

				inputFields.css({

					borderColor : primaryblue

				});
			} else if (colorAttr == 'primary-green') {

				inputFields.css({

					borderColor : primarygreen

				});
			} else if (colorAttr == 'primary-red') {

				inputFields.css({

					borderColor : primaryred

				});
			} else if (colorAttr == 'primary-yellow') {

				inputFields.css({

					borderColor : primaryyellow

				});
			} else if (colorAttr == 'primary-orange') {

				inputFields.css({

					borderColor : primaryorange

				});
			} else if (colorAttr == 'primary-lightblue') {

				inputFields.css({

					borderColor : primarylightblue

				});
			} else if (colorAttr == 'primary-purple') {

				inputFields.css({

					borderColor : primarypurple

				});
			} else if (colorAttr == 'primary-mint') {

				inputFields.css({

					borderColor : primarymint

				});
			} else if (colorAttr == 'primary-lightorange') {

				inputFields.css({

					borderColor : primarylightorange

				});
			} else if (colorAttr == 'primary-turquoise') {

				inputFields.css({

					borderColor : primaryturquoise

				});
			} else if (colorAttr == 'primary-lightpink') {

				inputFields.css({

					borderColor : primarylightpink

				});
			} else if (colorAttr == 'primary-beige') {

				inputFields.css({

					borderColor : primarybeige

				});
			} else if (colorAttr == 'primary-everywalk') {

				inputFields.css({

					borderColor : primaryeverywalk

				});
			}
		});
	}

})(jQuery);