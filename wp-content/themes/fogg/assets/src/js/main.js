/*
  ============================================

  File: main.js
  Type: Javascript
  Version: 1.0.0
  Author: Mike Stopford (Fogg Associates 2015)
  Project: Fogg Associates website

  ============================================
  
  Table of contents:
  ------------------

    []  jQuery document ready

      []  $('.col').matcheHeight;
      []  headerSearchFocus();
      []  homepageMainSection();
      []  smoothScroll();
      []	projectBoxCaption();
      []	mobileMenu();
      []	teamMemberProfiles();
      []	ajaxPostLoad();


    []  Window load

      []


    []  Window resize

      []


    []  Window scroll

      []


    []  Functions

      []  isValidEmailAddress();
      []  addressMapOverlay();
      []  initializeMap();
      []  listingResultsQty();


*/


$(document).ready(function(){

  $('.col').matchHeight();

  headerSearchFocus();
  homepageMainSection();
  smoothScroll();
  projectBoxCaption();
  mobileMenu();
  teamMemberProfiles();
  ajaxPostsLoad();

  var $container = $('.isotope-container');
  // initialize
  // $container.masonry({
  //   itemSelector: '.pod-outer',
  //   percentPosition: true,
  //   columnWidth: '.grid-sizer'
  // });
  // 

	$container.isotope({
    itemSelector: '.pod-outer',
    masonry: {
      columnWidth: '.grid-sizer'
    }
  });

	$filters = $('.filter-button-group');

  $filters.on( 'click', 'button', function() {
	  var filterAttr = $(this).attr('data-filter');
	  // set filter in hash
    location.hash = 'filter=' + encodeURIComponent( filterAttr );

    console.log(location.hash);

	  $container.isotope({ filter: filterAttr });
	});

	var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    if ( !hashFilter && isIsotopeInit ) {
      return;
    }
    isIsotopeInit = true;
    // filter isotope
    $container.isotope({
      itemSelector: '.pod-outer',
      filter: hashFilter,
      masonry: {
        columnWidth: '.grid-sizer'
      }
    });
    // set selected class on button
    if ( hashFilter ) {
      $filters.find('.active').removeClass('active');
      $filters.find('[data-filter="' + hashFilter + '"]').addClass('active');
    }
  }

  $(window).on( 'hashchange', onHashchange );
  // trigger event handler to init Isotope
  onHashchange();

	$('.filter-btn').on('click', function() {
		$(this).siblings().removeClass('active');
		$(this).toggleClass('active');
	});

	// $('a').on('click', function(e) {
	// 	e.preventDefault();

	// 	newLocation = this.href;
 //  	$('body').fadeOut(300, newPage);

 //  });

 //  function newPage() {
 //  	window.location = newLocation;
 //  }
 

  $(".profile-individual").css({ maxHeight: $(window).height()});

  var homepageVideo = $('#homepage-bg-video');

  if ($('body').hasClass('admin-bar')) {
    $('header').addClass('with-admin-bar');
  } else {
    if ($('header').hasClass('with-admin-bar')) {
      $('header').removeClass('with-admin-bar');
    }

  }

  homepageVideo.tubular({videoId: '0pu0JlyiDKU'}); // where idOfYourVideo is the YouTube ID.

  console.log('test');

});

$(window).load(function(){

  var scroll = $(window).scrollTop();
	var windowWidth = $(window).width();

   //>=, not <=
  if (scroll >= 20 && windowWidth > 600) {
    $('header').addClass('scrolled');
  }

  $('img.lazy').show().lazyload({
    effect: 'fadeIn'
  });
});

$(window).resize(function(){

  homepageMainSection();

});

$(window).scroll(function() {
	var scroll = $(window).scrollTop();
  var windowWidth = $(window).width();

   // >=, not <=
  if (scroll >= 20 && windowWidth > 600) {
  	$('header').addClass('scrolled');
  } else {
  	$('header').removeClass('scrolled');
  }

}); // END window.scroll();


/* --------------------------------------------------------------
functions
-------------------------------------------------------------- */

function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

/* check for valid email address */
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}

function headerSearchFocus() {

  var searchBar = $('#header-search');
  var searchInput = searchBar.find('input[type=search]');

  searchInput.on('focus', function() {
    searchBar.addClass('focus');
  });

  searchInput.on('focusout', function() {
    if ( searchInput.val() == '') {
        searchBar.removeClass('focus');
    }
  });

}

function homepageMainSection() {

  var windowHeight = $(window).height();
  var windowWidth = $(window).width();
  var mainSection = $('#homepage-main');

  // if (windowWidth >= 600) {
    mainSection.css({
      'height' : windowHeight
    });
  // }

}

function smoothScroll() {

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

}

function projectBoxCaption() {

  var caption = $('.project-box-caption');
  var project = $('.project-box');
  var closeBtn = $('.project-box-close');

  project.on('click', function() {
    // project.find(caption).fadeOut();
    $(this).find(caption).fadeIn();
  });

  closeBtn.on('click',function(e) {
    e.stopPropagation();
    e.preventDefault();
    // $(this).closest(caption).fadeOut();
    // 
    $(this).closest(caption).animate({'top':'100%'}, 300, 'easeInBack').fadeOut().animate({'top':'0'});

  });

  closeBtn.on('mousedown', function() {
    $(this).find('img').animate({width:"20px", height:"20px"},100);
  });
  closeBtn.on('mouseup', function() {
    $(this).find('img').animate({width:"30px", height:"30px"},100);
  });

}

function mobileMenu() {
  
    var menuBtn = $('.mobile-menu-btn');
    $(".mobile-menu").css({ maxHeight: $(window).height()});

    menuBtn.on('click', function(e) {

      e.preventDefault();

      if ($(this).hasClass('closed') || $(this).hasClass('default') ) {
        $(this).removeClass('closed');
        $(this).removeClass('default');
        $(this).addClass('open');
        $('.mobile-menu').addClass('active');
      } else {
        $(this).removeClass('open');
        $(this).addClass('closed');
        $('.mobile-menu').removeClass('active');
      }

    });

    $('.menu-item-has-children').append('<span class="open-sub-menu">+</span>');

    $('.open-sub-menu').on('click', function(e) {

      e.preventDefault();
      e.stopPropagation();

      console.log('clicked');

      $(this).siblings('.sub-menu').toggleClass('active');
      $(this).siblings('.sub-menu').slideToggle();

      if ($(this).siblings('.sub-menu').hasClass('active')) {
        $(this).text('-');
      } else {
        $(this).text('+');
      }

    });
}

function initializeMap() {

  var locations = {

    fogg: {
      lat: 53.458521,
      lang: -2.622164
    }

  }

  var markerImage = php_vars + '/assets/images/design/icons/fogg-marker-2.png';

  var locationSelected = $(this).data('location');

  var mapCanvas = document.getElementById('map-loader');

  var mapOptions = {
    center: new google.maps.LatLng(locations.fogg.lat, locations.fogg.lang),
    zoom: 16,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    scaleControl: false,
    panControl: false,
    streetViewControl: false
  }

  var map = new google.maps.Map(mapCanvas, mapOptions);

  var marker = new google.maps.Marker({
    position: mapOptions.center,
    map: map,
    title: 'Fogg Associates',
    animation: google.maps.Animation.DROP,
    icon: markerImage
  });

  map.set('styles', [
    {
      featureType:'road',
      elementType: 'geometry',
      stylers: [
        { color: '#f8f6f6' },
        { weight: 0.8 }
      ]
    },
    {
      featureType:'road',
      elementType: 'labels',
      stylers: [
        { saturation: 0 },
        { color: '#8ec44a' },
        { weight: 0.2 }
      ]
    },
    {
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [
          { color: '#ffffff' },
        ]
    },
    {
        featureType: 'poi.business',
        elementType: 'labels',
        stylers: [
          { color: '#292961' },
          { weight: 0.4 },
          { visibility: 'off' }
        ]
    }
  ]); 

};

google.maps.event.addDomListener(window, 'load', initializeMap);

function teamMemberProfiles() {

  var profilePhoto = $('.team-profile-grid');
  var profilePhoto = $('.team-profile-grid');
  var profileCanvas = $('.profile-individual');
  var profileCanvasInner = $('.profile-individual .inner-content');

  var closeBtn = $('.close-btn');

  profilePhoto.on('click', function(e) {

    e.preventDefault();
    e.stopPropagation();

    var member = $(this).data('team-member');

    $('.profile-individual[data-team-member-individual=' + member + ']').fadeIn().addClass('open');

  });

  $('body').on('click', function() {
    profileCanvas.fadeOut().removeClass('open');
  });

  profileCanvasInner.on('click', function(e) {
    e.stopPropagation();
  });

  closeBtn.on('click', function(e) {
    e.preventDefault();
    profileCanvas.fadeOut().removeClass('open');
  });

}


function ajaxPostsLoad() {
    var page = 1;
    var loading = true;
    var $loadMore = $('.load-more-posts');
    var $content = $(".blog-posts");
    var load_posts = function(){
        $.ajax({
            type       : "GET",
            data       : {numPosts : 6, pageNumber: page},
            dataType   : "html",
            url        : "/wp-content/themes/fogg/loop-handler.php",
            beforeSend : function(){
                    if(page != 1){
                        $content.append('<div id="temp_load" style="text-align:center">\
                            <img src="/wp-content/themes/fogg/assets/images/design/icons/ajax-loader.gif" />\
                            </div>');
                    }
                },
            success : function(data){
                $data = $(data);
                if($data.length){
                    $data.hide();
                    $content.append($data);
                    $data.slideDown(500, function(){
                        $("#temp_load").remove();
                        loading = false;
                    });
                } else {
                    $("#temp_load").remove();
                }
            },
            error     : function(jqXHR, textStatus, errorThrown) {
                $("#temp_load").remove();
                alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
    }
    $loadMore.on('click', function(e) {
        e.preventDefault();
        loading = true;
        page++;
        load_posts();
    });
    load_posts();
}