<?php 

    // Our include
    define('WP_USE_THEMES', false);
    require_once('../../../wp-load.php');

    // Our variables
    $numPosts = (isset($_GET['numPosts'])) ? $_GET['numPosts'] : 0;
    $page = (isset($_GET['pageNumber'])) ? $_GET['pageNumber'] : 0;

    $loop = new  WP_Query(array(
        // 'post_type' => 'post',
        // 'offset' => 1,
        'posts_per_page' => $numPosts,
        'paged' => $page
        )); 

    if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <?php get_template_part('post-layout', get_post_type( $post ) ); ?>
    
<!-- Stop The Loop (but note the "else:" - see next line). -->
 <?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
 <?php 
    endif;
    wp_reset_query();
 ?>