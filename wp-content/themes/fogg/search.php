<?php 
    get_template_part('/shared/header');
?>
<section class="clear-header standard-padding-x">

    <div class="container">
        <div class="with-sidebar col width-10">

            <?php if (have_posts() ) while (have_posts() ) :the_post(); ?>

                <?php get_template_part('post-layout', get_post_type( $post ) ); ?>

            <?php endwhile; ?>

        </div>

        <?php get_sidebar(); ?>
    </div>

</section>

<?php get_template_part('/shared/footer'); ?>