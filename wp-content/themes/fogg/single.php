<?php 
    get_template_part('/shared/header');
?>
<section class="standard-padding-x clearfix clear-header blog-article">

    <div class="container">
        <!-- .with-sidebar -->
        <div class="col width-10 with-sidebar">
            <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="blog-post-image">
            <?php if (get_field('blog_post_video_embed')) : ?>
                <iframe width="100%" height="510" src="<?php the_field('blog_post_video_embed'); ?>" frameborder="0" allowfullscreen></iframe>
            <?php elseif ( get_field('post_masthead') ) : ?>
                <img src="<?php the_field('post_masthead'); ?>">
            <?php else : ?>
                <?php the_post_thumbnail(); ?>
            <?php endif; ?>
            </div>

            <div class="blog-post-details">
                <div class="blog-post-heading">
                    <h3><?php the_title(); ?></h3>
                    <p class="blog-post-author"><?php the_author(); ?></p>
                    <p class="blog-post-date"><?php the_time('l, F j, Y'); ?></p>
                </div>

                <?php
                    // original content display
                        // the_content();
                    // split content into array
                        $content = split_content();
                    // output first content section in column1
                        echo '<div class="blog-post-content single-col">', array_shift($content), '</div>';
                    // output remaining content sections in column2
                        echo '<div class="blog-post-content single-col">', implode($content), '</div>';
                ?>
            </div>
            <?php 
                endwhile;
                endif;
                wp_reset_query();
             ?>

        </div>
        <!-- // .with-sidebar -->

        <?php get_sidebar(); ?>
    </div>

</section>
<?php get_template_part('/shared/footer'); ?>