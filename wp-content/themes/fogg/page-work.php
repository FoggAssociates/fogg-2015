<?php 
    get_template_part('/shared/header');
    /*
        Template name: Work
     */
?>

<div class="button-group work-filters filter-button-group container clear-header">
<?php

    $taxonomy = 'work_categories';
    $queried_term = get_query_var($taxonomy);
    $terms = get_terms($taxonomy, 'slug='.$queried_term);
    if($terms) { ?>

    <button class="filter-btn" data-filter="*">Show all</button>

        <? foreach($terms as $term) {
            $term_name = $term->name;
            $term_lower = strtolower($term_name);
            $termsToReplace = array('&amp; ', ' ');
            $replacements = array('', '-');
            $term_display = str_replace($termsToReplace, $replacements, $term_lower);
            ?>
            <button class="filter-btn" data-filter=".<?php echo $term_display; ?>"><?php echo $term_name; ?></button>
<?php
        }
    }
?>
</div>

<section id="work" class="work-holder isotope-container container standard-padding-x">
	<div class="grid-sizer"></div>
    <?php $childpages = fogg_projects_get_child_pages(); ?>
    
    <?php if ( $childpages->have_posts() ) while ( $childpages->have_posts() ) : $childpages->the_post();
        $termsObj = get_the_terms( $post->ID, 'work_categories' );
     ?>
    <div class="pod-outer <?php the_field('project_grid_width'); ?> <?php the_field('project_grid_height'); ?> <?php
        if($termsObj) {
            foreach($termsObj as $term) {
                $term_name = $term->name;
                $term_lower = strtolower($term_name);
                $termsToReplace = array('&amp; ', ' ');
                $replacements = array('', '-');
                $term_display = str_replace($termsToReplace, $replacements, $term_lower);
                echo $term_display . ' ';
            }
        }
    ?>">
        <!-- .pod -->
        <div class="pod project-box <?php the_field('project_colour'); ?>-bg">

            <div class="project-box-caption">
                <div class="project-box-caption-inner">

                    <a href="#" class="project-box-close">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/design/icons/close.png" alt="close">
                    </a>

                    <div class="project-box-heading">
                        <h3><?php the_title(); ?></h3>
                    </div>

                    <div class="project-box-summary">
                        <p><?php the_field('project_summary'); ?>...</p>
                    </div>

                    <a href="<?php the_permalink(); ?>" class="project-box-btn">View project</a>

                    <div class="project-box-tags">
                        <?php
                    
                            if($termsObj) {

                                foreach($termsObj as $term) {

                                    $term_name = $term->name;
                                    $term_link = get_term_link($term, 'work_categories');

                                 ?>
                                    <a class="project-box-tag" href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a>

                        <?php   }

                            }
                        ?>
                    </div>

                </div>
            </div>

            <?php

                $pgImage = get_field('project_grid_image');
                $pgImagePath = $pgImage['sizes']['large'];

                $mhImage = get_field('project_masthead_image');
                $mhImagePath = $mhImage['sizes']['large'];

            ?>
            <?php if ($pgImage) : ?>
                <img class="pod-background-image" src="<?php echo $pgImagePath; ?>" alt="<?php the_title(); ?>">
            <?php else : ?>
                <img class="pod-background-image" src="<?php echo $mhImagePath; ?>" alt="<?php the_title(); ?>">
            <?php endif; ?>
        </div>
        <!-- // .pod -->
    </div>
    <?php endwhile; wp_reset_query(); ?>

</section>

<?php get_template_part('/shared/footer'); ?>