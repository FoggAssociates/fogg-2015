<!-- .blog-post -->
<a href="<?php the_permalink(); ?>" class="blog-post grid-view">
    <!-- .push-right -->
    <div class="push-right">
        <div class="blog-post-details blog-post-match">
            <div class="blog-post-heading">
                <h3><?php the_title(); ?></h3>
                <p class="blog-post-author"><?php the_author(); ?></p>
                <p class="blog-post-date"><?php the_time('l, F j, Y'); ?></p>
                <p class="blog-post-summary"><?php echo get_the_blog_excerpt(); ?>...</p>
                <button class="standard-btn green-bg">Read Post</button>
            </div>
        </div>
        <div class="blog-post-image blog-post-match">
            <?php the_post_thumbnail(); ?>
        </div>
    </div>
    <!-- // .push-right -->
</a>
<!-- .blog-post -->