		<!-- footer -->
		<footer>
			<div class="container">
				<div class="col width-3">
					<p class="heading">Fogg Associates</p>
					<p>UK based international brand development firm. Est. 2008</p>
				</div>

				<div class="col width-3">
					
					<div class="col width-6">
						<?php wp_nav_menu(array('theme_location' => 'footer-menu-1')) ?>
					</div>

					<div class="col width-6">
						<?php wp_nav_menu(array('theme_location' => 'footer-menu-2')) ?>
					</div>

				</div>

				<div class="col width-3">
					<p class="heading">The Elms, 152a High Street, WA12 9SG</p>
					<p>T. +44 (0) 1925 226 139</p>
					<p>E. info@foggassociates.com</p>
				</div>

				<div class="col width-3">
					<p class="heading">SHARE, FOLLOW &amp; CONNECT</p>
					<a target="_blank" href="https://twitter.com/foggassociates" class="footer-social"><span class="icon-twitter"></span></a>
					<!-- <a target="_blank" href="#" class="footer-social"><span class="icon-feed2"></span></a> -->
					<a target="_blank" href="https://www.linkedin.com/company/fogg-associates" class="footer-social"><span class="icon-linkedin2"></span></a>
					<a target="_blank" href="https://www.youtube.com/channel/UCyIXu2tEDO9XWf6wuFY7kig" class="footer-social"><span class="icon-youtube4"></span></a>
				</div>
			</div>
		</footer>
		<!-- END footer -->
		<?php wp_footer(); ?>
	</body>
</html>