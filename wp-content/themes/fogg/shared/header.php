<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php the_title(); ?> | <?php echo get_bloginfo('name'); ?> - <?php echo get_bloginfo('description'); ?></title>

        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>

	<nav class="mobile-menu">
		<?php wp_nav_menu(array('theme_location' => 'mobile-menu')) ?>
	</nav>
	<!-- header -->
	<header>
		<div class="container">

			<div class="mobile-menu-btn default">
				<div class="bar-horizontal left"></div>
				<div class="bar-horizontal fade-out"></div>
				<div class="bar-horizontal right"></div>
			</div>

			<!-- .float-left -->
			<div class="float-left">
			<?php if ( get_theme_mod('fogg_logo') ) : ?>
				<a href="/" class="logo"><img src="<?php echo get_theme_mod('fogg_logo'); ?>" alt="fogg associates"></a>
			<?php endif; ?>
			</div>

			<!-- .float-right -->
			<div class="float-right">
				<nav id="header-navigation">
					<?php wp_nav_menu(array('theme_location' => 'header-menu')) ?>
				</nav>
				<!-- <form id="header-search">
					<input type="text" placeholder="Search..." name="search" id="header-search-bar">
					<input type="submit" value="" id="header-search-btn">
				</form> -->

				<form id="header-search" class="search" method="get" action="<?php echo home_url(); ?>" role="search">

				  <input type="search" id="header-search-bar" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				  
				  <button type="submit" id="header-search-btn" role="button" class="btn btn-default right"/><span class="glyphicon glyphicon-search white"></span></button>

				</form>

			</div>

		</div>
	</header>
	<!-- END header -->