<?php
	function fogg_customize_register( $wp_customize ) {
		$wp_customize->add_setting( 'fogg_logo' ); // Add setting for logo uploader

		// Add control for logo uploader (actual uploader)
	    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fogg_logo', array(
	        'label'    => __( 'Upload Fogg Associates Logo', 'fogg' ),
	        'section'  => 'title_tagline',
	        'settings' => 'fogg_logo',
	    ) ) );
	}
	add_action( 'customize_register', 'fogg_customize_register' );
?>