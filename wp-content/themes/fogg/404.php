<?php 
    get_template_part('/shared/header');
    /*
        Template name: 404 page
     */
?>

<section class="clear-header" id="project-masthead">

    <img class="full-width" src="<?php echo site_url(); ?>/wp-content/uploads/2015/07/Error-404.gif" alt="Page not found">

</section>

<section class="double-padding-x clearfix">

    <div class="container">
       
            <h3>Sorry, we can't find that page</h3>
            <p>Please return to the <a href="/">homepage</a> or check out our work and blog posts below:</p>

            <div class="about-page-content">
            <?php
                // original content display
                    // the_content();
                // split content into array
                    $content = split_content();
                // output first content section in column1
                    echo '<div class="about-page-content-col">', array_shift($content), '</div>';
                // output remaining content sections in column2
                    echo '<div class="about-page-content-col">', implode($content), '</div>';
            ?>
            </div>
   
    </div>

</section>

<section id="homepage-work" class="isotope-container container">

    <?php 

    // $args = array(

    //         'post_type' => array(
    //             'page', 'post', 'fogg_messaging'
    //             ),
    //         'posts_per_page' => 16,
    //         'orderby' => 'rand'

    //     );

    // $loop = new WP_Query( $args );
    
    $projects = array( 'post_type' => 'page', 'posts_per_page' => -1, 'post_parent' => 7, 'orderby' => 'rand' );
    $blog_posts = array( 'post_type' => 'post', 'posts_per_page' => -1, 'orderby' => 'rand' );    
    $messaging_stickies = array('post_type' => 'fogg_messaging', 'posts_per_page' => -1, 'meta_key' =>'message_sticky', 'meta_value' => '1');
    $messaging = array( 'post_type' => 'fogg_messaging', 'posts_per_page' => -1,'meta_key' => 'message_sticky', 'meta_value' => '1', 'meta_compare' => '!=', 'orderby' => 'rand' );   
  
    $projects_query = new WP_Query($projects);    
    $blog_posts_query = new WP_Query($blog_posts);    
    $messaging_stickies_query = new WP_Query($messaging_stickies);
    $messaging_query = new WP_Query($messaging);
    
    $result = new WP_Query();

    // shuffle($blog_posts_query, $projects_query, $messaging_query);
    $tempArr = array_merge( $messaging_query->posts, $projects_query->posts, $blog_posts_query->posts );
    shuffle($tempArr);
   
     $tempArr2 = array_merge($messaging_stickies_query->posts, $tempArr);
   
    $result->posts = $tempArr2;
    $result->post_count = count($result->posts); ?>

    <div class="grid-sizer"></div>
    <?php if ( $result->have_posts() ) while ( $result->have_posts() ) : $result->the_post(); ?>
    
    <!-- .pod-outer -->
    <div class="pod-outer <?php ( get_post_type() == 'page') ? the_field('project_grid_width') : the_field('blog_grid_width'); ?> <?php ( get_post_type() == 'page') ? the_field('project_grid_height') : the_field('blog_grid_height'); ?> <?php echo (get_post_type() == 'fogg_messaging') ? 'single-width single-height' : ''; ?>">
        <!-- .pod -->
        <div class="pod project-box <?php

            $blogColour = get_field('blog_post_grid_colour');
            $pageColour = get_field('project_colour');
            $messageColor = get_field('background_color');

            if (get_post_type() == 'fogg_messaging') :
                echo 'message-box ' . $messageColor . '-bg';
            elseif (get_post_type() == 'post') :
                echo 'blog-box ' . $blogColour . '-bg';
            else :
                echo $pageColour . '-bg';
            endif;

         ?>
         ">

            <?php if (get_post_type() == 'post') : ?>

                <a class="blog-pod-title" href="<?php the_permalink(); ?>">
                    <p class="title"><?php the_title(); ?></p>
                    <p class="author-info"><?php the_author(); ?></p>
                </a>

            <?php endif; ?>

            <?php if (get_post_type() == 'fogg_messaging') : ?>

                <?php if (get_field('message_link')) : ?><a class="no-underline" href="<?php the_field('message_link'); ?>"><?php endif; ?><?php the_content(); ?><?php if (get_field('message_link')) : ?></a><?php endif; ?>

            <?php endif; ?>

            <?php if (get_post_type() == 'page') : ?> 
            <div class="project-box-caption">
                <div class="project-box-caption-inner">

                    <a href="#" class="project-box-close">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/design/icons/close.png" alt="close">
                    </a>

                    <div class="project-box-heading">
                        <h3><?php the_title(); ?></h3>
                    </div>

                    <div class="project-box-summary">
                        <p><?php the_field('project_summary'); ?>...</p>
                    </div>

                    <a href="<?php the_permalink(); ?>" class="project-box-btn">View project</a>

                    
                        <div class="project-box-tags">
                            <?php
                                $termsObj = get_the_terms( $post->ID, 'work_categories' );

                                if($termsObj) {

                                    foreach($termsObj as $term) {

                                        $term_name = $term->name;
                                        $term_link = get_term_link($term, 'work_categories');

                                     ?>
                    
                                        <a class="project-box-tag" href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a>

                            <?php   }

                                }
                            ?>
                        </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if (get_post_type() != 'fogg_messaging' && get_post_type() != 'post') : ?> 
                <img class="lazy pod-background-image" data-original="<?php ( get_post_type() == 'page') ? 
                (get_field('project_grid_image')) ? the_field('project_grid_image') : the_field('project_masthead_image') : the_field('post_masthead'); ?>" alt="<?php the_title(); ?>">
            <?php endif; ?>
        </div>
        <!-- // .pod -->
    </div>
    <!-- // .pod-outer -->

    <?php endwhile; wp_reset_query(); ?>

</section>

<?php get_template_part('/shared/footer'); ?>