<?php
/* Horwich Farrelly functions file */

	add_theme_support( 'post-thumbnails' ); 


	// split content at the more tag and return an array
	function split_content() {
	    global $more;
	    $more = true;
	    $content = preg_split('/<span id="more-\d+"><\/span>/i', get_the_content('more'));
	    for($c = 0, $csize = count($content); $c < $csize; $c++) {
	        $content[$c] = apply_filters('the_content', $content[$c]);
	    }
	    return $content;
	}

	/*
	 * enqueue scripts and styles
	 */
	function fogg_scripts() {

		wp_enqueue_style( 'global-styles', get_template_directory_uri() . '/assets/build/css/main.css', array(), '1.0.0', false );

		wp_enqueue_script( 'modernizr-script', get_template_directory_uri() . '/assets/src/js/vendor/modernizr-2.6.2.min.js', array(), '1.0.0', false );

	    wp_enqueue_script( 'global-jquery', get_template_directory_uri() . '/assets/src/js/vendor/jquery-1.10.2.min.js', array(), true );

	    wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDwnnw3E0Zl12BAB4_TW7l_9vuWyNUNEd8', array(), true );

	    wp_enqueue_script( 'global-match-height', get_template_directory_uri() . '/assets/src/js/vendor/jquery.matchHeight.min.js', array('global-jquery'), '1.0.0', true );

	    wp_enqueue_script( 'global-ui', get_template_directory_uri() . '/assets/build/js/min/jquery-ui.min.js', array('global-jquery'), '1.0.0', true );

	    wp_enqueue_script( 'global-classie', get_template_directory_uri() . '/assets/build/js/min/classie.js', array('global-jquery'), '1.0.0', true );

	    // wp_enqueue_script( 'global-masonry', get_template_directory_uri() . '/assets/build/js/min/masonry.pkgd.min.js', array('global-jquery'), '1.0.0', true );

	    wp_enqueue_script( 'global-lazyload', get_template_directory_uri() . '/assets/build/js/min/jquery.lazyload.min.js', array('global-jquery'), '1.0.0', true );

	    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/src/js/vendor/isotope.pkgd.min.js', array('global-jquery'), '1.0.0', true );
	    wp_enqueue_script( 'tubular', get_template_directory_uri() . '/assets/src/js/vendor/jquery.tubular.1.0.js', array('global-jquery'), '1.0.0', true );

		wp_enqueue_script( 'global-scripts', get_template_directory_uri() . '/assets/build/js/min/main.min.js', array('global-jquery'), '1.0.0', true );

		$templateDirectory = get_template_directory_uri();
		$teamMemberPhoto = the_field('team_member_photo');
		wp_localize_script( 'global-scripts', 'php_vars', $templateDirectory );
		wp_localize_script( 'global-scripts', 'team_member_photo', $teamMemberPhoto );

	}

	add_action( 'wp_enqueue_scripts', 'fogg_scripts' );

	/*
	 * set up custom logo upload
	*/
	require get_template_directory() . '/shared/customizer.php';

	/*
	 * set up custom post type
	*/
	function create_post_type() {

	  register_post_type( 'fogg_team',
	    array(
	      'labels' => array(
	        'name' => __( 'Team' ),
	        'singular_name' => __( 'Team member' ),
	        'add_new' => __( 'Add Team member' ),
	        'add_new_item' => __( 'Add Team member' ),
	        'edit_item' => __( 'Edit Team member' ),
	        'new_item' => __( 'Team member' ),
	        'view' => __( 'View Team member' ),
	    		'view_item' => __( 'View Team member' ),
	    		'search_items' => __( 'Search team' ),
	    		'not_found' => __( 'No team members found' ),
	    		'not_found_in_trash' => __( 'No Team members found in Trash' ),
	    		'parent' => __( 'Parent Team member' )
	      ),
	      'public' => true,
	      'has_archive' => true,
	      'menu_icon' => '',
	    )
	  );
	  register_post_type( 'fogg_messaging',
	    array(
	      'labels' => array(
	        'name' => __( 'Brand messaging' ),
	        'singular_name' => __( 'Brand Message' ),
	        'add_new' => __( 'Add Brand Message' ),
	        'add_new_item' => __( 'Add Brand Message' ),
	        'edit_item' => __( 'Edit Brand Message' ),
	        'new_item' => __( 'Brand Message' ),
	        'view' => __( 'View Brand Message' ),
	    		'view_item' => __( 'View Brand Message' ),
	    		'search_items' => __( 'Search team' ),
	    		'not_found' => __( 'No Brand Messages found' ),
	    		'not_found_in_trash' => __( 'No Brand Messages found in Trash' ),
	    		'parent' => __( 'Parent Brand Message' )
	      ),
	      'public' => true,
	      'has_archive' => true,
	      'menu_icon' => '',
	    )
	  );
	}
	add_action( 'init', 'create_post_type' );

	function register_fogg_menu() {
	  register_nav_menu('header-menu',__( 'Header Menu' ));
	  register_nav_menu('footer-menu-1',__( 'Footer Menu 1' ));
	  register_nav_menu('footer-menu-2',__( 'Footer Menu 2' ));
	  register_nav_menu('mobile-menu',__( 'Mobile Menu' ));
	}
	add_action( 'init', 'register_fogg_menu' );


	/* widget area registration */
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name'=> 'Blog Sidebar',
			'id' => 'blog_sidebar',
			'before_widget' => '<div id="%1$s" class="sidebar-area %2$s">',
			'after_widget' => '</div>'
		));
	}

	function create_work_taxonomies() {
		$labels = array(
		    'name'              => _x( 'Work Categories', 'taxonomy general name' ),
		    'singular_name'     => _x( 'Work Category', 'taxonomy singular name' ),
		    'search_items'      => __( 'Search Work Categories' ),
		    'all_items'         => __( 'All Work Categories' ),
		    'parent_item'       => __( 'Parent Work Category' ),
		    'parent_item_colon' => __( 'Parent Work Category:' ),
		    'edit_item'         => __( 'Edit Work Category' ),
		    'update_item'       => __( 'Update Work Category' ),
		    'add_new_item'      => __( 'Add New Work Category' ),
		    'new_item_name'     => __( 'New Work Category Name' ),
		    'menu_name'         => __( 'Work Categories' ),
		);

		$args = array(
		    'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		    'labels'            => $labels,
		    'show_ui'           => true,
		    'show_admin_column' => true,
		    'query_var'         => true,
		    'rewrite'           => array( 'slug' => 'work_categories' ),
		);

		register_taxonomy( 'work_categories', array( 'page' ), $args );
		}

		add_action( 'init', 'create_work_taxonomies', 0 );

		function my_acf_admin_head()
	{
		?>
		<style type="text/css">

			.postbox *, .postbox *:before, .postbox *:after { 
			  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
			  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
			  box-sizing: border-box;         /* Opera/IE 8+ */
			}

			.postbox .field {
				background:#fff!important;
				padding:20px!important;
				margin-bottom:20px!important;
				border-bottom:3px solid;
				border-color:#666768;
			}

			#acf-project_grid_width, #acf-project_grid_height {
				width:50%;
				display:table-cell;
			}

			#acf-project_grid_width {
				border-right:3px solid #f2f2f2;
				border-bottom:6px solid #f2f2f2;
			}
			#acf-project_grid_height {
				border-left:3px solid #f2f2f2;
				border-bottom:6px solid #f2f2f2;
			}

			#acf-section_1_image_1.acf-conditional_logic-show, #acf-section_1_image_2.acf-conditional_logic-show, #acf-section_1_image_3.acf-conditional_logic-show, #acf-section_2_image_1.acf-conditional_logic-show, #acf-section_2_image_2.acf-conditional_logic-show, #acf-section_2_image_3.acf-conditional_logic-show, #acf-section_3_image_1.acf-conditional_logic-show, #acf-section_3_image_2.acf-conditional_logic-show, #acf-section_1_image_3.acf-conditional_logic-show, #acf-section_4_image_1.acf-conditional_logic-show, #acf-section_4_image_2.acf-conditional_logic-show, #acf-section_4_image_3.acf-conditional_logic-show {
				width:33%!important;
				display:inline-block!important;
				min-height:300px;
			}

			.preview-video {
				margin-top:20px;
			}

		</style>

		<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/build/js/input-custom.js"></script>
		<?php
	}

	add_action('acf/input/admin_head', 'my_acf_admin_head');


	function get_the_blog_excerpt(){

		$excerpt = get_the_content();
		// $excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$the_str = substr($excerpt, 0, 140);
		return $the_str;

	}

	/* return child pages for specific parents */
	function fogg_projects_get_child_pages() {
		global $post;
		$args = array(
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'posts_per_page' => -1,
			'post_type' => get_post_type( $post->ID ),
			'post_parent' => $post->ID
		);
		$childpages = new WP_Query($args);
		return $childpages;
	}		

	function add_menu_icons_styles(){
	?>
	 
	<style>
	#adminmenu .menu-icon-fogg_team div.wp-menu-image:before {
	  content: "\f307";
	}
	#adminmenu .menu-icon-fogg_messaging div.wp-menu-image:before {
	  content: "\f125";
	}
	</style>
	 
	<?php
	}
	add_action( 'admin_head', 'add_menu_icons_styles' );
?>