<?php 
    get_template_part('/shared/header');
    /*
        Template name: Project
     */
?>
<section class="clear-header" id="project-masthead">
	<?php 
		$mhImage = get_field('project_masthead_image');
	  $mhImagePath = $mhImage['sizes']['large'];
	 ?>
   <img src="<?php echo $mhImagePath; ?>" alt="<?php the_title(); ?>">

</section>

<section class="double-padding-x clearfix project-page-template project-color-<?php the_field('project_colour'); ?>">

    <div class="container">
       
       <div class="project-title">
            <h2 class="project-name"><?php the_title(); ?></h2>
            <p class="project-tagline"><?php the_field('project_tagline'); ?></p>


            <div class="project-tags">

            <?php
	            $termsObj = get_the_terms( $post->ID, 'work_categories' );

	            if($termsObj) {

	            	foreach($termsObj as $term) {

	            		$term_name = $term->name;
	            		$term_link = get_term_link($term, 'work_categories');

	            	 ?>
	
						<a href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a>

	        <?php 	}

	            }
			?>

            </div>
       </div>

       <div class="project-story">
			
			<!-- =============================================
			Project intro copy
			============================================== -->
			<!-- .row -->
            <div class="row">
				<?php
                    // original content display
                        // the_content();
                    // split content into array
                        $content = split_content();
                    // output first content section in column1
                        echo '<div class="col width-6">', array_shift($content), '</div>';
                    // output remaining content sections in column2
                        echo '<div class="col width-6">', implode($content), '</div>';
                ?>
			</div>
			<!-- // .row -->
			<!-- =============================================
			END Project intro copy
			============================================== -->

			<?php if ( get_field('special_promo_video')) { ?>
				<!-- .row -->
				<div class="row" id="video-embed">
					<iframe width="100%" height="500" src="<?php the_field('special_promo_video');?>?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<!-- // .row -->
			<?php } ?>

			<!-- =============================================
			Project image section 1
			============================================== -->
			<?php 
				$imageOrder = get_field('image_order');;
			?>
			<?php if ( $imageOrder == '1' || $imageOrder == '4' || $imageOrder == '5' ) : ?>
			
				<?php for ($x = 0; $x <= 1; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>


			<?php elseif ($imageOrder === '2' || $imageOrder === '3') : ?>

				<?php for ($x = 0; $x <= 2; $x++) { ?>

					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
						<!-- .row -->
						<div class="row">
							<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
							<div class="col width-12">
								<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } ?>
						</div>
						<!-- // .row -->
					<?php } ?>
				<?php } ?>

			<?php else : ?>

				<?php for ($x = 0; $x <= 1; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>
				
			<?php endif; ?>

			<!-- =============================================
			END Project image section 1
			============================================== -->

			<!-- =============================================
			Project info box
			============================================== -->
			<?php if ( get_field('project_info_boxes') > 0 ) { ?>

				<?php if ( get_field('project_info_box_1_text_alignment') == 'text-left' || get_field('project_info_box_1_text_alignment') == 'text-right' ) { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_1_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_1_header'); ?></h3>
							<?php the_field('project_info_box_1_text_content'); ?>
						</div>
						<div class="image">
							<?php if ( !get_field('project_info_box_1_quote')) { ?>
								<?php if (get_field('project_info_box_1_image')) : ?><img src="<?php the_field('project_info_box_1_image'); ?>"><?php endif; ?>
							<?php } else { ?>
								<blockquote>
									<span class="quote-mark">"</span><?php the_field('project_info_box_1_quote'); ?><span class="quote-mark">"</span>
								</blockquote>
							<?php } ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } else { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_1_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_1_header'); ?></h3>
						</div>
						<div class="image">
							<?php if (get_field('project_info_box_1_image')) : ?><img src="<?php the_field('project_info_box_1_image'); ?>"><?php endif; ?>
						</div>
						<div class="text">
							<?php the_field('project_info_box_1_text_content'); ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } ?>

			<?php } ?>
			<!-- =============================================
			END Project info box
			============================================== -->

			<!-- =============================================
			Project quote box
			============================================== -->
			<?php if ( get_field('project_quote_box') ) { ?>

				<?php if ( get_field('project_quote_box_style') == 'text-full-width' ) { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 quote-block" style="background-image: url('<?php the_field('project_quote_box_background'); ?>');">
						<?php the_field('project_quote_box_text'); ?>
					</div>
				</div>
				<!-- // .row -->
				<?php } else { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 quote-block <?php echo ( get_field('project_quote_box_style') == 'text-left' ? 'text-left' : 'text-right' ); ?>">
						<?php the_field('project_quote_box_text'); ?>
						<img src="<?php the_field('project_quote_box_background'); ?>">
					</div>
				</div>
				<!-- // .row -->
				<?php } ?>

			<?php } ?>
			<!-- =============================================
			END Project quote box
			============================================== -->

			<!-- =============================================
			Project image section 2
			============================================== -->
			<?php 
				$imageOrder = get_field('image_order');;
			?>
			<?php if ( $imageOrder == '1' || $imageOrder == '5' ) : ?>
			
				<?php for ($x = 2; $x <= 2; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>


			<?php elseif ($imageOrder === '2') : ?>

				<?php for ($x = 3; $x <= 4; $x++) { ?>

					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
						<!-- .row -->
						<div class="row">
							<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
							<div class="col width-12">
								<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } ?>
						</div>
						<!-- // .row -->
					<?php } ?>
				<?php } ?>

			<?php elseif ($imageOrder === '3') : ?>

				<?php for ($x = 3; $x <= 4; $x++) { ?>

					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
						<!-- .row -->
						<div class="row">
							<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
							<div class="col width-12">
								<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } ?>
						</div>
						<!-- // .row -->
					<?php } ?>
				<?php } ?>

			<?php elseif ($imageOrder === '4') : ?>

				<?php for ($x = 1; $x <= 3; $x++) { ?>

					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
						<!-- .row -->
						<div class="row">
							<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
							<div class="col width-12">
								<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-6">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
							<div class="col width-4">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
							</div>
							<div class="col width-8">
								<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
							</div>
							<?php } ?>
						</div>
						<!-- // .row -->
					<?php } ?>
				<?php } ?>

			<?php else : ?>

				<?php for ($x = 2; $x <= 3; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>
				
			<?php endif; ?>

			<!-- =============================================
			END Project image section 2
			============================================== -->

			<!-- =============================================
			Project info box
			============================================== -->
			<?php if ( get_field('project_info_boxes') > 1 ) { ?>

				<?php if ( get_field('project_info_box_2_text_alignment') == 'text-left' || get_field('project_info_box_2_text_alignment') == 'text-right' ) { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_2_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_2_header'); ?></h3>
							<?php the_field('project_info_box_2_text_content'); ?>
						</div>
						<div class="image">
							<?php if ( !get_field('project_info_box_2_quote')) { ?>
								<img src="<?php the_field('project_info_box_2_image'); ?>">
							<?php } else { ?>
								<blockquote>
									<span class="quote-mark">"</span><?php the_field('project_info_box_2_quote'); ?><span class="quote-mark">"</span>
								</blockquote>
							<?php } ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } else { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_2_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_2_header'); ?></h3>
						</div>
						<div class="image">
							<img src="<?php the_field('project_info_box_2_image'); ?>">
						</div>
						<div class="text">
							<?php the_field('project_info_box_2_text_content'); ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } ?>

			<?php } ?>
			<!-- =============================================
			END Project info box
			============================================== -->

			<!-- =============================================
			Project image section 3
			============================================== -->
			<?php 
				$imageOrder = get_field('image_order');;
			?>
			<?php if ( $imageOrder == '1' ) : ?>
			
				<?php for ($x = 3; $x <= 3; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>

			<?php elseif ($imageOrder === '2' || $imageOrder === '4' ) : ?>

				<?php for ($x = 4; $x <= 4; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>

			<?php elseif ($imageOrder === '5' ) : ?>

				<?php for ($x = 3; $x <= 4; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>

			<?php elseif ($imageOrder === '3') : ?>

			<?php else : ?>

				<?php for ($x = 3; $x <= 3; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>
				
			<?php endif; ?>

			<!-- =============================================
			END Project image section 3
			============================================== -->

			<!-- =============================================
			Project info box
			============================================== -->
			<?php if ( get_field('project_info_boxes') > 2 ) { ?>

				<?php if ( get_field('project_info_box_3_text_alignment') == 'text-left' || get_field('project_info_box_3_text_alignment') == 'text-right' ) { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_3_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_3_header'); ?></h3>
							<?php the_field('project_info_box_3_text_content'); ?>
						</div>
						<div class="image">
							<?php if ( !get_field('project_info_box_3_quote')) { ?>
								<img src="<?php the_field('project_info_box_3_image'); ?>">
							<?php } else { ?>
								<blockquote>
									<span class="quote-mark">"</span><?php the_field('project_info_box_3_quote'); ?><span class="quote-mark">"</span>
								</blockquote>
							<?php } ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } else { ?>
				<!-- .row -->
				<div class="row">
					<div class="col width-12 text-info-area <?php the_field('project_info_box_3_text_alignment'); ?>">
						<div class="text">
							<h3><?php the_field('project_info_box_3_header'); ?></h3>
						</div>
						<div class="image">
							<img src="<?php the_field('project_info_box_3_image'); ?>">
						</div>
						<div class="text">
							<?php the_field('project_info_box_3_text_content'); ?>
						</div>
					</div>
				</div>
				<!-- // .row -->
				<?php } ?>

			<?php } ?>
			<!-- =============================================
			END Project info box
			============================================== -->

			<!-- =============================================
			Project image section 4
			============================================== -->
			<?php 
				$imageOrder = get_field('image_order');;
			?>
			<?php if ( $imageOrder == '1') : ?>
			
				<?php for ($x = 4; $x <= 4; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>

			<?php elseif ( $imageOrder == '2' || $imageOrder == '3' || $imageOrder == '4' || $imageOrder == '5') : ?>

			<?php else : ?>

				<?php for ($x = 4; $x <= 4; $x++) { ?>
					    
					<?php if ( get_field('project_image_section_'.$x.'') ) { ?>
					<!-- .row -->
					<div class="row">
						<?php if ( get_field('project_image_section_'.$x.'') == 'full-width' ) { ?>
						<div class="col width-12">
							<img class="<?php the_field('section_'.$x.'_image_1_display'); ?>" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'half-width' ) { ?>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-6">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'thirds-width' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_3'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'two-to-one' ) { ?>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } elseif ( get_field('project_image_section_'.$x.'') == 'one-to-two' ) { ?>
						<div class="col width-4">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_1'); ?>">
						</div>
						<div class="col width-8">
							<img class="full-width" src="<?php the_field('section_'.$x.'_image_2'); ?>">
						</div>
						<?php } ?>
					</div>
					<!-- // .row -->
					<?php } ?>

				<?php } ?>
				
			<?php endif; ?>

			<!-- =============================================
			END Project image section 4
			============================================== -->

			<?php if ( get_field('project_video_embed')) { ?>
				<!-- .row -->
				<div class="row" id="video-embed">
					<iframe width="100%" height="500" src="<?php the_field('project_video_embed');?>?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<!-- // .row -->
			<?php } ?>
       </div>
            
    </div>

</section>

<?php get_template_part('/shared/footer'); ?>