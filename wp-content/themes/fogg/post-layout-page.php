<div class="pod-outer double-width single-height">
    <!-- .pod -->
    <div class="pod project-box <?php the_field('project_colour'); ?>-bg">

        <div class="project-box-caption">
            <div class="project-box-caption-inner">

                <a href="#" class="project-box-close">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/design/icons/close.png" alt="close">
                </a>

                <div class="project-box-heading">
                    <h3><?php the_title(); ?></h3>
                </div>

                <div class="project-box-summary">
                    <p><?php the_field('project_summary'); ?></p>
                </div>

                <a href="<?php the_permalink(); ?>" class="project-box-btn">View</a>

            </div>
        </div>

        <img class="pod-background-image" src="<?php the_field('project_masthead_image'); ?>" alt="<?php the_title(); ?>">
    </div>
    <!-- // .pod -->
</div>