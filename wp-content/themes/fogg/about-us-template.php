<?php 
    get_template_part('/shared/header');
    /*
        Template name: About us
     */
?>
<section class="clear-header" id="project-masthead">
	<?php if (get_field('about_us_video')) : ?>

	    <iframe width="100%" height="390" src="<?php the_field('about_us_video');?>" frameborder="0" allowfullscreen></iframe>

	 <?php else : ?>

	 	<img class="full-width" src="<?php the_field('about_us_image'); ?>" alt="<?php the_title(); ?>">

	 <?php endif; ?>

</section>

<section class="double-padding-x clearfix">

    <div class="container">
       
            <h2 class="project-name"><?php the_title(); ?></h2>

			<div class="about-page-content">
            <?php
                // original content display
                    // the_content();
                // split content into array
                    $content = split_content();
                // output first content section in column1
                    echo '<div class="about-page-content-col">', array_shift($content), '</div>';
                // output remaining content sections in column2
                    echo '<div class="about-page-content-col">', implode($content), '</div>';
            ?>
            </div>
   
    </div>

</section>

<?php get_template_part('/shared/footer'); ?>