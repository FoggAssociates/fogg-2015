<?php 
    get_template_part('/shared/header');
    /*
        Template name: Blog
     */
?>
<section id="blog-latest-slider" class="clear-header">

    <div class="container">

        <?php 
            query_posts('post_type=post&posts_per_page=1'); 
            if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- .individual-slide -->
        <div class="individual-slide">
            <div class="blog-post-details">
                <div class="blog-post-heading">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p class="blog-post-author">By <?php the_author(); ?></p>
                    <p class="blog-post-date"><?php the_time('l, F j, Y'); ?></p>
                    <a class="standard-btn green-bg" href="<?php the_permalink(); ?>">Read Post</a>
                </div>
            </div>

            <div class="blog-post-image">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            </div>
        </div>
        <!-- // .individual-slide -->
        <?php endwhile; else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
         <?php 
            endif;
            wp_reset_query();
         ?>

    </div>

</section>

<section class="double-padding-x clearfix">

    <div class="container">
        <!-- .with-sidebar -->
        <div class="style-as-col width-10 with-sidebar">

            <div class="blog-posts">

            </div>
            <a href="#" class="load-more-posts">Load more</a>
        </div>

        <!-- // .with-sidebar -->

        <?php get_sidebar(); ?>
    </div>

</section>

<?php get_template_part('/shared/footer'); ?>