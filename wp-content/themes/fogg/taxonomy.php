<?php 
    get_template_part('/shared/header');
?>
<section class="clear-header standard-padding-x">

    <div class="container">
        <div class="with-sidebar col width-10">

            <?php if (have_posts() ) while (have_posts() ) :the_post(); ?>

            <div class="taxonomy-listing <?php the_field('project_colour'); ?>-bg">

				<div class="float-left width-4">
					<a href="<?php the_permalink(); ?>">
					<?php
		                $mhImage = get_field('project_masthead_image');
		                $mhImagePath = $mhImage['sizes']['medium'];
		            ?>
						<img src="<?php echo $mhImagePath; ?>" alt="<?php the_title(); ?>">
					</a>
				</div>
				<div class="float-left width-8">
	                <div class="project-box-heading">
	                    <h3><?php the_title(); ?></h3>
	                </div>

	                <div class="project-box-summary">
	                    <p><?php the_field('project_summary'); ?>...</p>
	                </div>

	                <a href="<?php the_permalink(); ?>" class="standard-btn green-bg">View</a>
	            </div>
			</div>

            <?php endwhile; ?>

        </div>

        <?php get_sidebar(); ?>
    </div>

</section>

<?php get_template_part('/shared/footer'); ?>